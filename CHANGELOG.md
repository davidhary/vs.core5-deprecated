# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [7.0.7704] - 2021-02-02
Conversion of isr.Core to .NET 5.0.

(C) 2012 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[7.0.7704]: (https://bitbucket.org/davidhary/vs.core5/src/main/)
