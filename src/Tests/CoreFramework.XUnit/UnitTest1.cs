using System;
using Xunit;

using Xunit.Abstractions;


namespace isr.Core.Framework.XUnit
{
    public class UnitTest1
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public UnitTest1( ITestOutputHelper output )
        {
            this._Output = output;
        }


        [Fact]
        public void Test1()
        {
            this._Output.WriteLine( "Testing write line" );
        }
    }
}
