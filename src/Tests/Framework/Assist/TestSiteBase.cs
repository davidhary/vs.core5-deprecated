// file:	Framework\Assist\TestSiteBase.cs
//
// summary:	Implements the test site base class
using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{

    /// <summary> Test site base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract partial class TestSiteBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TestSiteBase" /> class. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        protected TestSiteBase() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        /// resources;
        /// False if this method releases only unmanaged
        /// resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this._TraceMessagesQueueListener is object )
                        {
                            this._TraceMessagesQueueListener.Dispose();
                        }

                        this._TraceMessagesQueueListener = null;
                    }

                    this._TraceMessagesQueues.Clear();
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " VALIDATIONS "

        /// <summary> Validated test context. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A TestContext. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        [CLSCompliant( false )]
        public TestContext ValidatedTestContext( TestContext testContext )
        {
            return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : testContext;
        }

        #endregion

        #region " APPLICATION DOMAIN DATA DIRECTORY "

        /// <summary> The name of the data directory application domain property. </summary>
        public const string ApplicationDomainDataDirectoryPropertyName = "DataDirectory";

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void ModifyApplicationDomainDataDirectoryPath()
        {
            this.ModifyApplicationDomainDataDirectoryPath( System.IO.Path.GetDirectoryName( System.Reflection.Assembly.GetExecutingAssembly().Location ) );
        }

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file.
        /// </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void ModifyApplicationDomainDataDirectoryPath( string path )
        {
            AppDomain.CurrentDomain.SetData( ApplicationDomainDataDirectoryPropertyName, path );
        }

        /// <summary> Reads application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> The application domain data directory path. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string ReadApplicationDomainDataDirectoryPath()
        {
            string value = AppDomain.CurrentDomain.GetData( ApplicationDomainDataDirectoryPropertyName ) as string;
            return value ?? string.Empty;
        }

        #endregion

    }

}
