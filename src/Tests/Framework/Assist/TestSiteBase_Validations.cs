// file:	Framework\Assist\TestSiteBase_Validations.cs
//
// summary:	Implements the test site base validations class

using System;
using System.Data;
using System.Data.Common;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{
    public abstract partial class TestSiteBase
    {

        /// <summary> Validated data row. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A DataRow. </returns>
        [Obsolete( "The .Net Core version of the adapter does not support DataSource yet." )]
        public static DataRow ValidatedDataRow( TestContext testContext )
        {
            return testContext is object
                ? throw new NotSupportedException( "https://github.com/Microsoft/testfx/issues/233 The .Net Core version of the adapter does not support DataSource yet. As a workaround, you could update to the latest beta and use https://github.com/Microsoft/testfx-docs/blob/master/RFCs/006-DynamicData-Attribute.md instead." )
                : null;
            // return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : ValidatedDataRow( testContext.DataRow );
        }

        /// <summary> Validated data row. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataRow"> The data row. </param>
        /// <returns> A DataRow. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public static DataRow ValidatedDataRow( DataRow dataRow )
        {
            return dataRow is null ? throw new ArgumentNullException( nameof( dataRow ) ) : dataRow;
        }

        /// <summary> Validated connection. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <returns> A DbConnection. </returns>
        [Obsolete( "The .Net Core version of the adapter does not support DataSource yet." )]
        public static DbConnection ValidatedConnection( TestContext testContext )
        {
            return testContext is object
                ? throw new NotSupportedException( "https://github.com/Microsoft/testfx/issues/233 The .Net Core version of the adapter does not support DataSource yet. As a workaround, you could update to the latest beta and use https://github.com/Microsoft/testfx-docs/blob/master/RFCs/006-DynamicData-Attribute.md instead." )
                : null;
            // return testContext is null ? throw new ArgumentNullException( nameof( testContext ) ) : Validatedconnection( testContext.DataConnection );
        }

        /// <summary> Validated connection. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> A connection. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        [CLSCompliant( false )]
        public static DbConnection Validatedconnection( DbConnection connection )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection;
        }

    }
}
