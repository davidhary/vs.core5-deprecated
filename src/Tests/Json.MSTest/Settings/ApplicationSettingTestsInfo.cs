using System;
using System.Diagnostics;

namespace isr.Core.FrameworkTests
{
    /// <summary> Application settings tests settings. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    internal class ApplicationSettingsTestsInfo : isr.Core.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public ApplicationSettingsTestsInfo() : base( Settings.BuildAppSettingsFileName() )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public bool Verbose { get; set; }

        /// <summary>
        /// Gets or sets the sentinel indicating of all data are to be used for a test.
        /// </summary>
        /// <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
        public bool All { get; set; }

        /// <summary> True if the test set is enabled. </summary>
        /// <value> The enabled option. </value>
        public bool Enabled { get; set; }

        #endregion

        #region " CONFIGURATION SETTINGS "

        /// <summary>   The other String value. </summary>
        public const string StringValueOther = "A";

        /// <summary>   The string value expected. </summary>
        public const string StringValueExpected = "a";

        /// <summary>   Gets or sets the String value. </summary>
        /// <value> The String value. </value>
        public string StringValue { get; set; }

        /// <summary>   The other integer value. </summary>
        public const int IntegerValueOther = 0;

        /// <summary>   The integer value expected. </summary>
        public const int IntegerValueExpected = 1;

        /// <summary>   Gets or sets the integer value. </summary>
        /// <value> The integer value. </value>
        public int IntegerValue { get; set; }

        /// <summary>   The expected boolean value. </summary>
        public const bool BooleanValueExpected = true;

        /// <summary>   the other boolean value. </summary>
        public const bool BooleanValueOther = false;

        /// <summary>   Gets or sets a boolean value. </summary>
        /// <value> True if boolean value, false if not. </value>
        public bool BooleanValue { get; set; }

        /// <summary>   The byte value expected. </summary>
        public const byte ByteValueExpected = ( byte ) 8;

        /// <summary>   The other byte value. </summary>
        public const byte ByteValueOther = ( byte ) 0;

        /// <summary>   Gets or sets the byte value. </summary>
        /// <value> The byte value. </value>
        public byte ByteValue { get; set; }

        /// <summary>   The timespan value expected. </summary>
        public static TimeSpan TimespanValueExpected() { return TimeSpan.Parse( "00:00:02.8250000" ); }

        /// <summary>   Timespan other value. </summary>
        /// <remarks>   David, 2021-01-28. </remarks>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan TimespanValueOther() { return TimeSpan.Zero; }

        /// <summary>   Gets or sets the timespan value. </summary>
        /// <value> The timespan value. </value>
        public global::System.TimeSpan TimespanValue { get; set; }

        /// <summary>   The date time value expected. </summary>
        public static DateTime DateTimeValueExpected() { return System.DateTime.Parse( "2020-10-10 10:10:20" ); }

        /// <summary>   The date time other value. </summary>
        public static DateTime DateTimeValueOther() { return System.DateTime.MinValue; }

        /// <summary>   Gets or sets the date time value. </summary>
        /// <value> The date time value. </value>
        public DateTime DateTimeValue { get; set; }

        /// <summary>   The trace event type value expected. </summary>
        public const TraceEventType TraceEventTypeValueExpected = TraceEventType.Information;

        /// <summary>   The other trace event type. </summary>
        public const TraceEventType TraceEventTypeValueOther = TraceEventType.Verbose;

        /// <summary>   Gets or sets the trace event type value. </summary>
        /// <value> The trace event type value. </value>
        public TraceEventType TraceEventTypeValue { get; set; }

        #endregion

    }
}
