using System;
using System.Drawing.Imaging;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.FrameworkTests
{
    /// <summary>
    /// This is a test class for MyAssemblyInfoTest and is intended
    /// to contain all MyAssemblyInfoTest Unit Tests
    /// </summary>
    [TestClass()]
    public class ApplicationSettingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #endregion

        public static string AppSettingsFileName
        {
            get {
                var name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                return System.IO.Path.Combine( AppDomain.CurrentDomain.BaseDirectory, isr.Core.Json.JsonSettingsBase.BuildAppSettingsFileName( name ) );
            }
        }

        /// <summary>   (Unit Test Method) application settings file name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationSettingsFileNameShouldEqual()
        {
            Settings jsonSettings = new Settings();
            Assert.AreEqual( ApplicationSettingTests.AppSettingsFileName, jsonSettings.SettingsFullFileName() );
        }

        /// <summary>   (Unit Test Method) application settings section name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationSettingsSectionNameShouldEqual()
        {
            Settings jsonSettings = new Settings();
            Assert.AreEqual( nameof(Settings), jsonSettings.SectionName() );
        }

        [TestMethod()]
        public void ApplicationTestSiteSettingsFileNameShouldEqual()
        {
            TestSiteSettings jsonSettings = new TestSiteSettings();
            Assert.AreEqual( ApplicationSettingTests.AppSettingsFileName, jsonSettings.SettingsFullFileName() );
        }

        /// <summary>   (Unit Test Method) application settings section name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationTestSiteSettingsSectionNameShouldEqual()
        {
            TestSiteSettings jsonSettings = new TestSiteSettings();
            var sectionName = nameof( TestSiteSettings )[0..^8];
            Assert.AreEqual( sectionName, jsonSettings.SectionName() );
        }

        /// <summary>
        /// (Unit Test Method) queries if a given application setting file should exists.
        /// </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ApplicationSettingFileShouldExists()
        {
            Settings jsonSettings = new Settings();
            var fi = new System.IO.FileInfo( jsonSettings.SettingsFullFileName() );
            Assert.IsTrue( fi.Exists, $"{fi.FullName} should exists" );
        }

        /// <summary>   (Unit Test Method) application setting file should save. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ApplicationSettingShouldSave()
        {
            // toggle boolean value and see if it is saved.
            var boolValue = AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize;
            AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize = !boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( !boolValue, AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize,
                             $"{nameof( Settings.CheckUnpublishedMessageLogFileSize )} Changed value should be saved and read" );
            AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize = boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( boolValue, AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize,
                             $"{nameof( Settings.CheckUnpublishedMessageLogFileSize )} Restored value should be saved and read" );
        }

        [TestMethod()]
        public void ApplicationSettingTestInfoShouldSave()
        {
            // toggle boolean value and see if it is saved.
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = !boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( !boolValue, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue, $"Changed value should be saved and read" );
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( boolValue, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue, $"Restored value should be saved and read" );

        }

        /// <summary>   (Unit Test Method) boolean application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void BooleanApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.BooleanValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue );
        }

        /// <summary>   (Unit Test Method) boolean application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void BooleanApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = ApplicationSettingsTestsInfo.BooleanValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.BooleanValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = boolValue;
        }

        /// <summary>   (Unit Test Method) string setting value should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringSettingValueShouldEqual()
        {
            var expectedDummyValue = "Dummy";
            Assert.AreEqual( expectedDummyValue, AppSettings.Instance.Settings.Dummy );
        }


        /// <summary>   (Unit Test Method) String application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.StringValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue );
        }

        /// <summary>   (Unit Test Method) String application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue = ApplicationSettingsTestsInfo.StringValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.StringValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Integer application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void IntegerApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.IntegerValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue );
        }

        /// <summary>   (Unit Test Method) Integer application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void IntegerApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue = ApplicationSettingsTestsInfo.IntegerValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.IntegerValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Byte application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ByteApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.ByteValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue );
        }

        /// <summary>   (Unit Test Method) Byte application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ByteApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue = ApplicationSettingsTestsInfo.ByteValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.ByteValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Timespan application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TimespanApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.TimespanValueExpected(), AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue );
        }

        /// <summary>   (Unit Test Method) Timespan application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TimespanApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue = ApplicationSettingsTestsInfo.TimespanValueOther();
            Assert.AreEqual( ApplicationSettingsTestsInfo.TimespanValueOther(), AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue = boolValue;
        }

        /// <summary>   (Unit Test Method) DateTime application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void DateTimeApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.DateTimeValueExpected(), AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue );
        }

        /// <summary>   (Unit Test Method) DateTime application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void DateTimeApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue = ApplicationSettingsTestsInfo.DateTimeValueOther();
            Assert.AreEqual( ApplicationSettingsTestsInfo.DateTimeValueOther(), AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue = boolValue;
        }

        /// <summary>   (Unit Test Method) TraceEventType application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TraceEventTypeApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.TraceEventTypeValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue );
        }

        /// <summary>   (Unit Test Method) TraceEventType application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TraceEventTypeApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue = ApplicationSettingsTestsInfo.TraceEventTypeValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.TraceEventTypeValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue = boolValue;
        }

    }
}
