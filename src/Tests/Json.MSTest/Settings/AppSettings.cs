// file:	Framework\Assist\AppSettings.cs
//
// summary:	Implements the application settings class

using System;
using System.IO;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

namespace isr.Core.FrameworkTests
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings
    {

        #region " CONSTRUCTION "

        private AppSettings()
        {
            this.Settings = new Settings();
            this.TestSite = new TestSiteSettings();
            this.ApplicationSettingsTestsInfo = new ApplicationSettingsTestsInfo();
            this.ReadSettings();
        }

        private static readonly object Instancelock = new object();

        private static AppSettings _Instance = null;

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance
        {
            get {
                if ( AppSettings._Instance == null )
                {
                    lock ( AppSettings.Instancelock )
                    {
                        if ( AppSettings._Instance == null )
                        {
                            AppSettings._Instance = new AppSettings();
                        }
                    }
                }
                return AppSettings._Instance;
            }
        }

        #endregion

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        public void ReadSettings()
        {
            if ( this.Settings.Exists() )
            {
                this.Settings.ReadSettings();
                this.TestSite.ReadSettings();
                this.ApplicationSettingsTestsInfo.ReadSettings();
            }
        }

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public void SaveSettings()
        {
            this.Settings.SaveSettings( this );
        }

        /// <summary>   Gets the filename of the application settings file. </summary>
        /// <value> The filename of the application settings file. </value>
        public static string AppSettingsFileName()
        {
            var name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            return System.IO.Path.Combine( AppDomain.CurrentDomain.BaseDirectory, $"{name}.settings.json" );
        }

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSite { get; }

        /// <summary>   Gets the information describing the application settings tests. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> Information describing the application settings tests. </value>
        internal ApplicationSettingsTestsInfo ApplicationSettingsTestsInfo { get; }

    }

}
