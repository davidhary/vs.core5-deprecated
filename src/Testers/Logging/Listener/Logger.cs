// file:	Logging\Listener\Logger.cs
//
// summary:	Implements the logger class

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace isr.Core.Logging.Listener
{

    /// <summary>   Interface for log listener. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public interface ILogListener
    {
        /// <summary>   Process the log described by Info. </summary>
        /// <param name="info"> The information. </param>
        void ProcessLog( LogEntry info );
    }

    /// <summary>   A logger. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public static class Logger
    {
        /// <summary>   The synchronize lock. </summary>
        private static readonly object SyncLock = new object();

        /// <summary>   The active. </summary>
        private static int _Active = 0;

        /// <summary>   The listeners. </summary>
        private static readonly List<ILogListener> Listeners = new List<ILogListener>();

        /// <summary>   Logs. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="info"> The information. </param>
        public static void Log( LogEntry info )
        {
            lock ( SyncLock )
            {
                if ( Active )
                {
                    foreach ( ILogListener listener in Listeners )
                    {

                        _ = Task.Run( () => {

                            try
                            {
                                listener.ProcessLog( info );
                            }
                            catch // (Exception ex)
                            {
                            }

                        } );

                    }

                }

            }
        }
        /// <summary>   Logs. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="level">        The level. </param>
        /// <param name="text">         The text. </param>
        /// <param name="exception">    (Optional) The exception. </param>
        /// <param name="source">       (Optional) Source for the. </param>
        /// <param name="eventId">      (Optional) Identifier for the event. </param>
        public static void Log( LogLevel level, string text, Exception exception = null, string source = "", int eventId = 0 )
        {
            LogEntry Info = new LogEntry( level, text, exception, source, eventId );
            Log( Info );
        }

        /// <summary>   Log information. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="text">     The text. </param>
        /// <param name="source">   (Optional) Source for the. </param>
        /// <param name="eventId">  (Optional) Identifier for the event. </param>
        public static void Info( string text, string source = "", int eventId = 0 )
        {
            Log( LogLevel.Info, text, null, source, eventId );
        }
        /// <summary>   Warns. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="text">     The text. </param>
        /// <param name="source">   (Optional) Source for the. </param>
        /// <param name="eventId">  (Optional) Identifier for the event. </param>
        public static void Warn( string text, string source = "", int eventId = 0 )
        {
            Log( LogLevel.Warn, text, null, source, eventId );
        }
        /// <summary>   Errors. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="exception">    The exception. </param>
        /// <param name="source">       (Optional) Source for the. </param>
        /// <param name="eventId">      (Optional) Identifier for the event. </param>
        public static void Error( Exception exception, string source = "", int eventId = 0 )
        {
            Log( LogLevel.Error, null, exception, source, eventId );
        }

        /// <summary>   Adds Listener. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="listener"> The listener to remove. </param>
        public static void Add( ILogListener listener )
        {
            lock ( SyncLock )
            {
                try
                {
                    if ( !Listeners.Contains( listener ) )
                    {
                        Listeners.Add( listener );
                    }
                }
                catch
                {
                }
            }

        }
        /// <summary>   Removes the given Listener. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="listener"> The listener to remove. </param>
        public static void Remove( ILogListener listener )
        {
            lock ( SyncLock )
            {
                try
                {
                    if ( Listeners.Contains( listener ) )
                    {
                        _ = Listeners.Remove( listener );
                    }
                }
                catch
                {
                }
            }
        }

        /// <summary>   Gets or sets a value indicating whether the active. </summary>
        /// <value> True if active, false if not. </value>
        public static bool Active
        {
            get {
                lock ( SyncLock )
                {
                    return _Active == 0;
                }
            }
            set {
                lock ( SyncLock )
                {
                    if ( !value )
                        _Active++;
                    else
                    {
                        _Active--;
                        if ( _Active < 0 )
                            _Active = 0;
                    }
                }
            }
        }
    }
}
