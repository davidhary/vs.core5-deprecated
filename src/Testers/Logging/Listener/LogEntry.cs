
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace isr.Core.Logging.Listener
{

    /// <summary>   A log entry. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public class LogEntry
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        public LogEntry()
        {
            this.TimeStampUtc = DateTime.UtcNow;
            this.UserName = Environment.UserName;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="level">        The level. </param>
        /// <param name="text">         The text. </param>
        /// <param name="exception">    (Optional) The exception. </param>
        /// <param name="source">       (Optional) The source. </param>
        /// <param name="eventId">      (Optional) The identifier of the event. </param>
        public LogEntry( LogLevel level, string text, Exception exception = null, string source = "", int eventId = 0 )
            : this()
        {
            this.Level = level;
            this.Text = text;
            this.Exception = exception;
            this.Source = source;
            this.EventId = eventId;
        }

        /// <summary>   Name of the static host. </summary>
        public static readonly string StaticHostName = System.Net.Dns.GetHostName();

        /// <summary>   Gets or sets the name of the user. </summary>
        /// <value> The name of the user. </value>
        public string UserName { get; private set; }


        /// <summary>   Gets the name of the host. </summary>
        /// <value> The name of the host. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string HostName => StaticHostName;

        /// <summary>   Gets or sets the Date/Time of the time stamp UTC. </summary>
        /// <value> The time stamp UTC. </value>
        public DateTime TimeStampUtc { get; private set; }

        /// <summary>   Gets or sets the source for the. </summary>
        /// <value> The source. </value>
        public string Source { get; set; }

        /// <summary>   Gets or sets the level. </summary>
        /// <value> The level. </value>
        public LogLevel Level { get; set; }

        /// <summary>   Gets or sets the text. </summary>
        /// <value> The text. </value>
        public string Text { get; set; }

        /// <summary>   Gets or sets the exception. </summary>
        /// <value> The exception. </value>
        public Exception Exception { get; set; }

        /// <summary>   Gets or sets the identifier of the event. </summary>
        /// <value> The identifier of the event. </value>
        public int EventId { get; set; }
    }

    /// <summary>   Values that represent log levels. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public enum LogLevel
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None = 0,
        /// <summary>   An enum constant representing the Information option. </summary>
        Info = 1,
        /// <summary>   An enum constant representing the Warning option. </summary>
        Warn = 2,
        /// <summary>   An enum constant representing the error option. </summary>
        Error = 3,
    }

}
