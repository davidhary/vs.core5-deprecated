// file:	Logging\Listener\LogFileListener.cs
//
// summary:	Implements the log file listener class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace isr.Core.Logging.Listener
{
    /// <summary>   A log file listener. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public class LogFileListener : ILogListener
    {
        /// <summary>   List of widths of the columns. </summary>
        private readonly Dictionary<string, int> _ColumnWidths = new Dictionary<string, int>();

        /// <summary>   Applies the retain policy. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private void ApplyRetainPolicy()
        {
            FileInfo FI;
            try
            {
                List<FileInfo> FileList = new DirectoryInfo( this.FolderName )
                .GetFiles( "*.log", SearchOption.TopDirectoryOnly )
                .OrderBy( fi => fi.CreationTime )
                .ToList();

                while ( FileList.Count >= this.RetainPolicyFileCount )
                {
                    FI = FileList.First();
                    FI.Delete();
                    _ = FileList.Remove( FI );
                }
            }
            catch
            {
            }

        }

        /// <summary>   The counter. </summary>
        private int _Counter = 0;

        /// <summary>   Writes a line. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="text"> The text. </param>
        private void WriteLine( string text )
        {
            // check the file size after any 100 writes
            this._Counter++;
            if ( this._Counter % 100 == 0 )
            {
                FileInfo FI = new FileInfo( this.LogFileFullName );
                if ( FI.Length > (1024 * 1024 * this.MaxFileSizeInMB) )
                {
                    this.BeginFile();
                }
            }

            File.AppendAllText( this.LogFileFullName, text );
        }

        /// <summary>   Pads. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="text">         The text. </param>
        /// <param name="maxLength">    The maximum length of the. </param>
        /// <returns>   A string. </returns>
        private static string Pad( string text, int maxLength )
        {
            return string.IsNullOrWhiteSpace( text )
                ? "".PadRight( maxLength )
                : text.Length > maxLength ? text.Substring( 0, maxLength ) : text.PadRight( maxLength );
        }

        /// <summary>   Prepare lengths. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private void PrepareLengths()
        {
            // prepare the lengths table
            this._ColumnWidths["Time"] = 24;
            this._ColumnWidths["Host"] = 16;
            this._ColumnWidths["User"] = 16;
            this._ColumnWidths["Level"] = 10;
            this._ColumnWidths["EventId"] = 12;
            this._ColumnWidths["Source"] = 32;
        }

        /// <summary>   Gets or sets the name of the log file full. </summary>
        /// <value> The name of the log file full. </value>
        public string LogFileFullName { get; private set; }

        /// <summary>   Begins a file. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private void BeginFile()
        {
            _ = Directory.CreateDirectory( this.FolderName );
            this.LogFileFullName = Path.Combine( this.FolderName, LogEntry.StaticHostName + "-" + DateTime.Now.ToString( "yyyyMMdd-HHmm" ) + ".log" );

            // titles
            StringBuilder SB = new StringBuilder();
            _ = SB.Append( Pad( "Time", this._ColumnWidths["Time"] ) );
            _ = SB.Append( Pad( "Host", this._ColumnWidths["Host"] ) );
            _ = SB.Append( Pad( "User", this._ColumnWidths["User"] ) );
            _ = SB.Append( Pad( "Level", this._ColumnWidths["Level"] ) );
            _ = SB.Append( Pad( "EventId", this._ColumnWidths["EventId"] ) );
            _ = SB.Append( Pad( "Source", this._ColumnWidths["Source"] ) );
            _ = SB.AppendLine( "Text" );

            File.WriteAllText( this.LogFileFullName, SB.ToString() );

            this.ApplyRetainPolicy();
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        public LogFileListener()
        {
            this.PrepareLengths();
            this.BeginFile();        // create the first file
        }

        /// <summary>   Process the log described by info. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="info"> The information. </param>
        public void ProcessLog( LogEntry info )
        {
            bool IsActive = info.Level != LogLevel.None
                            && this.Level != LogLevel.None
                            && Convert.ToInt32( this.Level ) <= Convert.ToInt32( info.Level );

            if ( IsActive )
            {
                StringBuilder builder = new StringBuilder();
                _ = builder.Append( Pad( info.TimeStampUtc.ToLocalTime().ToString( "yyyy-MM-dd HH:mm:ss.ff" ), this._ColumnWidths["Time"] ) );
                _ = builder.Append( Pad( info.HostName, this._ColumnWidths["Host"] ) );
                _ = builder.Append( Pad( info.UserName, this._ColumnWidths["User"] ) );
                _ = builder.Append( Pad( info.Level.ToString(), this._ColumnWidths["Level"] ) );
                _ = builder.Append( Pad( info.EventId > 0 ? info.EventId.ToString() : "", this._ColumnWidths["EventId"] ) );
                _ = builder.Append( Pad( info.Source, this._ColumnWidths["Source"] ) );

                string Text = info.Exception != null ? info.Exception.Message : info.Text;

                if ( !string.IsNullOrWhiteSpace( Text ) )
                {
                    _ = builder.Append( Text.Replace( "\r\n", " " ).Replace( "\r", " " ).Replace( "\n", " " ) );
                }

                _ = builder.AppendLine();
                this.WriteLine( builder.ToString() );
            }
        }

        /// <summary>   Gets or sets the level. </summary>
        /// <value> The level. </value>
        public LogLevel Level { get; set; } = LogLevel.Info;

        /// <summary>   Pathname of the folder. </summary>
        private string _FolderName = "./Logs";

        /// <summary>   Gets or sets the pathname of the folder. </summary>
        /// <value> The pathname of the folder. </value>
        public string FolderName
        {
            get => !string.IsNullOrWhiteSpace( this._FolderName ) ? this._FolderName : System.IO.Path.GetDirectoryName( this.GetType().Assembly.Location );
            set => this._FolderName = value;
        }

        /// <summary>   The maximum file size in megabytes. </summary>
        private int _MaxFileSizeInMB = 2;
        /// <summary>   Gets or sets the maximum file size in megabytes. </summary>
        /// <value> The maximum file size in megabytes. </value>
        public int MaxFileSizeInMB
        {
            get => this._MaxFileSizeInMB > 0 ? this._MaxFileSizeInMB : 2;
            set => this._MaxFileSizeInMB = value;
        }

        /// <summary>   Number of retain policy files. </summary>
        private int _RetainPolicyFileCount = 5;

        /// <summary>   Gets or sets the number of retain policy files. </summary>
        /// <value> The number of retain policy files. </value>
        public int RetainPolicyFileCount
        {
            get => this._RetainPolicyFileCount < 5 ? 5 : this._RetainPolicyFileCount;
            set => this._RetainPolicyFileCount = value;
        }
    }
}
