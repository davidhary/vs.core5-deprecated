// file:	Logging\Listener\Form1.cs
//
// summary:	Implements the form 1 class

using System;
using System.Threading;
using System.Windows.Forms;

using isr.Core.Logging.Listener;

namespace DesktopApp
{
    /// <summary>   A form 1. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    public partial class LogListenerForm : Form
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        public LogListenerForm()
        {
            this.InitializeComponent();
        }

        /// <summary>   Any click. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <exception cref="ApplicationException"> Thrown when an Application error condition occurs. </exception>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="ea">       Event information. </param>
        private void AnyClick( object sender, EventArgs ea )
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( this.LogInfoButton == sender )
                {
                    Logger.Info( "Stop playing with loggers" );
                }
                else if ( this.LogWarningButton == sender )
                {
                    Logger.Warn( "This is a last warning!!!" );
                }
                else if ( this.LogErrorButton == sender )
                {
                    try
                    {
                        throw new ApplicationException( "I told you already. No more playing with loggers" );
                    }
                    catch ( Exception ex )
                    {
                        Logger.Error( ex );
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.Error( ex );
            }
            finally
            {
                Thread.Sleep( 200 );
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>   Form initialize. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private void FormInitialize()
        {
            this.LogInfoButton.Click += this.AnyClick;
            this.LogWarningButton.Click += this.AnyClick;
            this.LogErrorButton.Click += this.AnyClick;

            Logger.Add( new LogFileListener() );
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            if ( !this.DesignMode )
                this.FormInitialize();
            base.OnShown( e );
        }

    }
}
