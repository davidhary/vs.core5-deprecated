namespace DesktopApp
{
    partial class LogListenerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogInfoButton = new System.Windows.Forms.Button();
            this.LogWarningButton = new System.Windows.Forms.Button();
            this.LogErrorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnInfo
            // 
            this.LogInfoButton.Location = new System.Drawing.Point(109, 12);
            this.LogInfoButton.Name = "btnInfo";
            this.LogInfoButton.Size = new System.Drawing.Size(75, 23);
            this.LogInfoButton.TabIndex = 0;
            this.LogInfoButton.Text = "Info";
            this.LogInfoButton.UseVisualStyleBackColor = true;
            // 
            // btnWarn
            // 
            this.LogWarningButton.Location = new System.Drawing.Point(109, 51);
            this.LogWarningButton.Name = "btnWarn";
            this.LogWarningButton.Size = new System.Drawing.Size(75, 23);
            this.LogWarningButton.TabIndex = 1;
            this.LogWarningButton.Text = "Warn";
            this.LogWarningButton.UseVisualStyleBackColor = true;
            // 
            // btnError
            // 
            this.LogErrorButton.Location = new System.Drawing.Point(109, 89);
            this.LogErrorButton.Name = "btnError";
            this.LogErrorButton.Size = new System.Drawing.Size(75, 23);
            this.LogErrorButton.TabIndex = 2;
            this.LogErrorButton.Text = "Error";
            this.LogErrorButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 154);
            this.Controls.Add(this.LogErrorButton);
            this.Controls.Add(this.LogWarningButton);
            this.Controls.Add(this.LogInfoButton);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LogInfoButton;
        private System.Windows.Forms.Button LogWarningButton;
        private System.Windows.Forms.Button LogErrorButton;
    }
}

