// file:	Logging\Main\Program.cs
//
// summary:	Implements the program class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.CommandLineUtils;
using Serilog;
using isr.Core.Logging.Filer;

namespace DesktopApp
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    internal class Program
    {
        /// <summary>   The main entry point for the application. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        [STAThread]
        private static void Main(string[] args )
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            var app = Program.CreateApp();
            Program.DefineOptions( app );
            Program.DefineArguments( app );
            Program.DefineExecution( app );
            try
            {
                // This begins the actual execution of the application
                Console.WriteLine( "Logging Tester executing..." );
                int outcome = app.Execute( args );
                Console.WriteLine( $"Logging Tester exited with code {outcome}" );
            }
            catch ( CommandParsingException ex )
            {
                // You'll always want to catch this exception, otherwise it will generate a messy and confusing error for the end user.
                // the message will usually be something like:
                // "Unrecognized command or argument '<invalid-command>'"
                Console.WriteLine( ex.Message );
                _ = MessageBox.Show( $"Exception; {Environment.NewLine}{ex.Message }.", "Exception occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch ( Exception ex )
            {
                Console.WriteLine( "Unable to execute application: {0}", ex.Message );
                _ = MessageBox.Show( $"Exception; {Environment.NewLine}{ex.Message }.", "Unable to execute application", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }

        }

        /// <summary>   Define execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        private static void DefineExecution( CommandLineApplication app )
        {

            // When no commands are specified, this block will execute.
            // This is the main "command"
            app.OnExecute( () => {

                //You can also use the Arguments collection to iterate through the supplied arguments
                int index = 0;
                foreach ( CommandArgument arg in app.Arguments )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Arguments collection [{index}] = {arg.Value ?? "null"}" );
                    index += 1;
                }

                foreach ( CommandOption opt in app.GetOptions() )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Option collection [{opt.LongName}] = {opt.Value() ?? "null"}" );
                }

                // Use the HasValue() method to check if the option was specified
                CommandOption basicOption = app.GetOptions().Where( x => x.ShortName == "o" ).FirstOrDefault();
                if ( basicOption is object && basicOption.HasValue() )
                {
                    Console.WriteLine( $"{nameof( basicOption )} selected value: {basicOption.Value()}" );
                    switch ( basicOption.Value() )
                    {
                        case "listener":
                            Application.Run( new LogListenerForm() { Text = $"Option: {basicOption.Value()}" } );
                            break;

                        case "filer":
                            RunFilerImplementation();
                            break;

                        case "serilog":
                            RunSerilogImplementation();
                            break;

                        default:
                            break;
                    }

                }
                else
                {
                    Console.WriteLine( "No options specified." );
                    // ShowHint() will display: "Specify --help for a list of available options and commands."
                    _ = MessageBox.Show( $"Logger option not specified; {Environment.NewLine}{app.GetHelpText()}.",
                                         "Logger Option Not Specified", MessageBoxButtons.OK, MessageBoxIcon.Information );
                }
                return 0;
            } );

        }

        #region " COMMAND LINE PARSING "

        /// <summary>   Creates the application. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <returns>   The new application. </returns>
        private static CommandLineApplication CreateApp()
        {
            // Instantiate the command line application
            var app = new CommandLineApplication {
                // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
                Name = "LoggingTester",
                Description = ".NET Core logging tester",
                ExtendedHelpText = @"This program demonstrates the logging functionality of the isr.Core.Logging.Logger."
            };

            // Set the arguments to display the description and help text
            _ = app.HelpOption( "-?|-h|--help" );

            // This is a helper/shortcut method to display version info - it is creating a regular Option, with some defaults.
            // The default help text is "Show version Information"
            _ = app.VersionOption( "-v|--version", () => {
                return $"Version {Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}";
            } );

            return app;
        }

        /// <summary>   Define application options. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>   A CommandOption. </returns>
        private static void DefineOptions( CommandLineApplication app )
        {
            // Arguments: -o listener -l error -d ..\_log -f Column

            // The first argument is the option template.
            // It starts with a pipe-delimited list of option flags/names to use
            // Optionally, It is then followed by a space and a short description of the value to specify.
            // e.g. here we could also just use "-o|--option"
            _ = app.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
            _ = app.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
            _ = app.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
            _ = app.Option( "-f|--form <value>", "Logging format: Column, Row, Tab, Comma", CommandOptionType.SingleValue );
        }

        /// <summary>   Enumerates define command line arguments in this collection. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process define command arguments in this
        /// collection.
        /// </returns>
        private static void DefineArguments( CommandLineApplication app )
        {
            // Arguments are basic arguments, that are parsed in the order they are given
            // e.g ConsoleArgs "first value" "second value"
            // This is OK for really simple tasks, but generally you're better off using Options
            // since they avoid confusion
            _ = app.Argument( "Option", "Listener or Provider Logging implementation" );
            _ = app.Argument( "Level", "Logging Level" );
            _ = app.Argument( "Directory", "Logging relative folder" );
            _ = app.Argument( "Format", "Logging format: Column, Rows, Tab, Comma" );
        }

        /// <summary>   Defines a simple command execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void DefineSimpleCommand( CommandLineApplication app )
        {
            // This is a command with no arguments - it just does default action.
            _ = app.Command( "simple-command", ( command ) => {
                //description and help text of the command.
                command.Description = "This is the description for simple-command.";
                command.ExtendedHelpText = "This is the extended help text for simple-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                command.OnExecute( () => {
                    Console.WriteLine( "simple-command is executing" );

                    //Do the command's work here, or via another object/method

                    Console.WriteLine( "simple-command has finished." );
                    return 0; //return 0 on a successful execution
                } );

            });
        }

        /// <summary>   Defines a complex command. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void DefineComplexCommand( CommandLineApplication app )
        {

            _ = app.Command( "complex-command", ( command ) => {
                // This is a command that has it's own options.
                command.ExtendedHelpText = "This is the extended help text for complex-command.";
                command.Description = "This is the description for complex-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                // There are 3 possible option types:
                // NoValue
                // SingleValue
                // MultipleValue

                // MultipleValue options can be supplied as one or multiple arguments
                // e.g. -m valueOne -m valueTwo -m valueThree
                var multipleValueOption = command.Option( "-m|--multiple-option <value>",
                    "A multiple-value option that can be specified multiple times",
                    CommandOptionType.MultipleValue );

                // SingleValue: A basic Option with a single value
                // e.g. -s sampleValue
                var singleValueOption = command.Option( "-s|--single-option <value>",
                    "A basic single-value option",
                    CommandOptionType.SingleValue );

                // NoValue are basically booleans: true if supplied, false otherwise
                var booleanOption = command.Option( "-b|--boolean-option",
                    "A true-false, no value option",
                    CommandOptionType.NoValue );

                command.OnExecute( () => {
                    Console.WriteLine( "complex-command is executing" );

                    // Do the command's work here, or via another object/method                    

                    // Grab the values of the various options. when not specified, they will be null.

                    // The NoValue type has no Value property, just the HasValue() method.
                    bool booleanOptionValue = booleanOption.HasValue();

                    // MultipleValue returns a List<string>
                    List<string> multipleOptionValues = multipleValueOption.Values;

                    // SingleValue returns a single string
                    string singleOptionValue = singleValueOption.Value();

                    // Check if the various options have values and display them.
                    // Here we're checking HasValue() to see if there is a value before displaying the output.
                    // Alternatively, you could just handle nulls from the Value properties
                    if ( booleanOption.HasValue() )
                    {
                        Console.WriteLine( "booleanOption option: {0}", booleanOptionValue.ToString() );
                    }

                    if ( multipleValueOption.HasValue() )
                    {
                        Console.WriteLine( "multipleValueOption option(s): {0}", string.Join( ",", multipleOptionValues ) );
                    }

                    if ( singleValueOption.HasValue() )
                    {
                        Console.WriteLine( "singleValueOption option: {0}", singleOptionValue ?? "null" );
                    }

                    Console.WriteLine( "complex-command has finished." );
                    return 0; // return 0 on a successful execution
                } );
            } );

        }

        #endregion

        #region " Filer IMPLEMENTATION "

        /// <summary>   Executes the 'provider implementation' operation. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        private static void RunFilerImplementation()
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureFilerServices( serviceCollection );

            // Create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

#if false
            ILogger<Program> logger = serviceProvider.GetService<ILoggerFactory>()
                                       .CreateLogger<Program>();

            // this used to log one line. No longer logging.
            logger.LogInformation( "Starting application..." );
#endif

            // Run the application
            serviceProvider.GetService<App>().Run();
        }

        private static void ConfigureFilerServices( IServiceCollection serviceCollection )
        {
            // Add logging
            _ = serviceCollection.AddLogging( loggingBuilder => {
                _ = loggingBuilder.AddFileLogger( options => {
                    options.LogLevel = LogLevel.Debug;
                    options.MaxFileSizeInMB = 5;
                    options.Folder = "../_Logs";
                } );
            } );

            // Add services
            _ = serviceCollection.AddTransient<ILoggingService, LoggingService>();

            // Add the application
            _ = serviceCollection.AddTransient<App>();
        }

#endregion

#region " SERILOG IMPLEMENTATION "

        private static void ConfigureSerilogServices( IServiceCollection serviceCollection )
        {
            // Add logging
            _ = serviceCollection.AddLogging( loggingBuilder => {
                                                                    _ = loggingBuilder.AddConsole();
                                                                    _ = loggingBuilder.AddSerilog();
                                                                    _ = loggingBuilder.AddDebug();
                                                                } );

            // Build configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath( AppContext.BaseDirectory )
                .AddJsonFile( "appsettings.json", false )
                .Build();

            // Initialize serilog logger
            Log.Logger = new LoggerConfiguration()
                 .ReadFrom.Configuration( configuration )
                 .CreateLogger();

            // Add access to generic IConfigurationRoot
            _ = serviceCollection.AddSingleton( configuration );

            // Add services
            _ = serviceCollection.AddTransient<ILoggingService, LoggingService>();

            // Add the application
            _ = serviceCollection.AddTransient<App>();
        }

        /// <summary>   Executes the 'provider implementation' operation. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        private static void RunSerilogImplementation()
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureSerilogServices( serviceCollection );

            // Create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            ILogger<Program> logger = serviceProvider.GetService<ILoggerFactory>()
                                       .CreateLogger<Program>();
            logger.LogInformation( "Starting application..." );

            // Run the application
            serviceProvider.GetService<App>().Run();
        }

#endregion

    }

#region " SERILOG IMPLEMENTATION CLASSES "

    // https://blog.bitscry.com/2017/05/31/logging-in-net-core-console-applications/
    // In .NET Core projects logging is managed via dependency injection. 
    // Whilst this works fine for ASP.NET projects where this is all automatically created upon starting a new project in Startup.cs, 
    // in console applications it takes a bit of configuration to get it up and running.


    public class App
    {
        private readonly ILoggingService _LoggingService;
        private readonly ILogger<App> _Logger;

        public App( ILoggingService loggingService, ILogger<App> logger )
        {
            this._LoggingService = loggingService;
            this._Logger = logger;
        }

#if false
        private readonly IConfigurationRoot _Config;
        public App( ILoggingService loggingService, IConfigurationRoot config, ILogger<App> logger )
        {
            this._LoggingService = loggingService;
            this._Logger = logger;
            this._Config = config;
        }
#endif

        public void Run()
        {
            this._Logger.LogInformation( $"Running application." );
            this._LoggingService.Run();
            // _ = System.Console.ReadKey();
        }
    }


    public interface ILoggingService
    {
        void Run();
    }

    internal class LoggingService : ILoggingService
    {
        private readonly ILogger<LoggingService> _Logger;

        public LoggingService( ILogger<LoggingService> logger )
        {
            this._Logger = logger;
        }

#if false
        private readonly IConfigurationRoot _Config;

        public LoggingService( ILogger<LoggingService> logger, IConfigurationRoot config )
        {
            this._Logger = logger;
            this._Config = config;
        }

#endif
        public void Run()
        {
            this._Logger.LogDebug( $"Running logging service." );
        }
    }

#endregion

}
