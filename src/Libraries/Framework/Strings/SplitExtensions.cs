namespace isr.Core.SplitExtensions
{
    /// <summary> Includes Split extensions for <see cref="System.String">String</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386. </para></remarks>
    public static class SplitExtensionMethods
    {

        /// <summary> The space. </summary>
        private const string _Space = " ";

        /// <summary> The space character. </summary>
        private const char _SpaceCharacter = ' ';

        /// <summary>
        /// Splits the string to words by adding spaces between lower and upper case characters.
        /// </summary>
        /// <remarks>
        /// Timing on I7-2600K 3.4GHz <para>
        /// CamelCase123 - 0.33ns</para><para>
        /// </para>
        /// </remarks>
        /// <param name="value"> The <c>String</c> value to split. </param>
        /// <returns> A <c>String</c> of words separated by spaces. </returns>
        public static string SplitWords( this string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }
            else
            {
                bool isSpace = false;
                bool isLowerCase = false;
                var newValue = new System.Text.StringBuilder();
                foreach ( char c in value )
                {
                    if ( !isSpace && isLowerCase && char.IsUpper( c ) )
                    {
                        _ = newValue.Append( _Space );
                    }

                    isSpace = c.Equals( _SpaceCharacter );
                    isLowerCase = !char.IsUpper( c );
                    _ = newValue.Append( c );
                }

                return newValue.ToString();
            }
        }

        /// <summary> A code standing for a regular expression element. </summary>
        private const string _Element = "$1";

        /// <summary> The complete split command finding case as well as numeric and string of upper case characters. </summary>
        /// <remarks>This will match against each word in Camel and Pascal case strings, while properly handling acronyms and including numbers. <para>
        /// (^[a-z]+)                               Match against any lower-case letters at the start of the command. </para><para>
        /// ([0-9]+)                                Match against one Or more consecutive numbers (anywhere in the string, including at the start). </para><para>
        /// ([A-Z]{1}[a-z]+)                        Match against Title case words (one upper case followed by lower case letters). </para><para>
        /// ([A-Z]+(?=([A-Z][a-z])|($)|([0-9])))    Match against multiple consecutive upper-case letters, leaving the last upper case letter
        /// out the match if it Is followed by lower case letters, and including it if it's followed
        /// by the end of the string or a number. </para><para>
        /// Timing on I7-2600K 3.4GHz </para><para>
        /// CamelCase123 - 2.8ns</para><para>
        /// </para></remarks>
        private const string _SplitCaseCommand = "((^[a-z]+)|([0-9_]+)|([A-Z]{1}[a-z]+)|([A-Z]+(?=([A-Z][a-z])|($)|([0-9_]))))";

        /// <summary>
        /// Splits camel or pascal case match against each word in Camel and Pascal case strings, while
        /// properly handling acronyms and including numbers.
        /// </summary>
        /// <remarks>
        /// Timing on I7-2600K 3.4GHz <para>
        /// CamelCase123 - 2.6ns</para><para>
        /// </para>
        /// </remarks>
        /// <param name="value"> The <c>String</c> value to split. </param>
        /// <returns> A String. </returns>
        public static string SplitCase( this string value )
        {
            return value.SplitCase( _Space );
        }

        /// <summary>
        /// Splits camel case match against each word in Camel and Pascal case strings, while properly
        /// handling acronyms and including numbers.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">     The <c>String</c> value to split. </param>
        /// <param name="delimiter"> The delimited. </param>
        /// <returns> A String. </returns>
        public static string SplitCase( this string value, string delimiter )
        {
            return System.Text.RegularExpressions.Regex.Replace( value, _SplitCaseCommand, $"{_Element}{delimiter}", System.Text.RegularExpressions.RegexOptions.Compiled ).Trim();
        }

        /// <summary> The complete split command finding case as well as numeric and string of upper case characters needing no trim action. </summary>
        /// <remarks> See https://www.codeproject.com/articles/108996/splitting-pascal-camel-case-with-regex-enhancement
        /// With a “Match if prefix is absent” group
        /// (https://docs.microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference?redirectedfrom=MSDN#grouping_constructs) <parap>
        /// containing the “beginning of line” character ^ prevents any matches from occurring on the first character,
        /// which eliminates the need for the String.Trim() call. </parap><parap>
        /// The formal name for this grouping construct is “Zero-width negative look-behind assertion”, but just think
        /// of it as “if you see what’s in here, don’t match the next thing”. </parap><parap>
        /// This was enhanced by adding split by numbers </parap><parap>
        /// Timing on I7-2600K 3.4GHz </parap><parap>
        /// CamelCase123 - 4.8ns</parap><parap>
        /// </parap> </remarks>
        private const string _SplitCaseSlowerCommand = "(?<!^)([A-Z][a-z]|(?<=[a-z])[^a-z]|(?<=[A-Z])[0-9_])";

        /// <summary> Splits a Pascal/Camel case string into words. </summary>
        /// <remarks>
        /// Timing on I7-2600K 3.4GHz <para>
        /// CamelCase123 - 4.8ns</para><para>
        /// </para>
        /// </remarks>
        /// <param name="value">     The <c>String</c> value to split. </param>
        /// <param name="delimiter"> The delimited. </param>
        /// <returns> A String. </returns>
        public static string SplitCaseSlower( this string value, string delimiter )
        {
            return System.Text.RegularExpressions.Regex.Replace( value, _SplitCaseSlowerCommand, $"{delimiter}{_Element}", System.Text.RegularExpressions.RegexOptions.Compiled );
        }

        /// <summary> Splits a Pascal/Camel case statement into words. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The <c>String</c> value to split. </param>
        /// <returns> A String. </returns>
        public static string SplitCaseSlower( this string value )
        {
            return value.SplitCaseSlower( _Space );
        }
    }
}
