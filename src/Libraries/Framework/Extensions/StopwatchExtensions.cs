using System;
using System.Diagnostics;
using System.Windows.Threading;

namespace isr.Core.StopwatchExtensions
{
    /// <summary> Includes extensions for <see cref="Stopwatch">Stop Watch</see>. </summary>
    /// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-19, 2.0.5556 </para></remarks>
    public static class StopwatchExtensionMethods
    {

        /// <summary> Gets or sets the microsecond per tick. </summary>
        /// <value> The microsecond per tick. </value>
        public static double MicrosecondPerTick { get; private set; } = 1000000.0d / Stopwatch.Frequency;

        /// <summary> Elapsed microseconds. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="stopwatch"> The stop watch. </param>
        /// <returns> A Long. </returns>
        public static double ElapsedMicroseconds( this Stopwatch stopwatch )
        {
            return stopwatch.ElapsedTicks * MicrosecondPerTick;
        }

        /// <summary> Gets or sets the millisecond per tick. </summary>
        /// <value> The millisecond per tick. </value>
        public static double MillisecondPerTick { get; private set; } = 1000.0d / Stopwatch.Frequency;

        /// <summary> Elapsed milliseconds. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="stopwatch"> The stop watch. </param>
        /// <returns> A Double. </returns>
        public static double ElapsedMilliseconds( this Stopwatch stopwatch )
        {
            return stopwatch.ElapsedTicks * MillisecondPerTick;
        }

        /// <summary> Query if 'stopwatch' is expired. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="stopwatch">       The stop watch. </param>
        /// <param name="timeoutTimespan"> The value. </param>
        /// <returns> <c>true</c> if expired; otherwise <c>false</c> </returns>
        public static bool IsExpired( this Stopwatch stopwatch, TimeSpan timeoutTimespan )
        {
            return stopwatch is object && timeoutTimespan > TimeSpan.Zero && stopwatch.Elapsed > timeoutTimespan;
        }

        /// <summary>   Waits while the stop watch is running until its elapsed time expires. </summary>
        /// <remarks>
        /// David, 2020-11-08. Uses the <see cref="Dispatcher"/> to allow events to occur during the wait
        /// time.
        /// </remarks>
        /// <param name="stopwatch">    The stop watch. </param>
        /// <param name="elapsedTime">  The elapsed time. </param>
        public static TimeSpan LetElapse( this Stopwatch stopwatch, TimeSpan elapsedTime )
        {
            return DispatcherExtensions.DispatcherExtensionMethods.LetElapse( Dispatcher.CurrentDispatcher, stopwatch, elapsedTime );
        }

    }
}
