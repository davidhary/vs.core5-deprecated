namespace isr.Core.NullableExtensions
{
    /// <summary> Includes extensions for <see cref="System.Nullable{T}">nullable</see>. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class NullableExtensionMethods
    {

        /// <summary> The null value. </summary>
        public const string NullValue = "NULL";

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this bool? value )
        {
            return $"{(value.HasValue ? value.Value ? "1" : "0" : NullValue)}";
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this byte? value )
        {
            return $"{(value.HasValue ? value.Value.ToString() : NullValue)}";
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this int? value )
        {
            return $"{(value.HasValue ? value.Value.ToString() : NullValue)}";
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this long? value )
        {
            return $"{(value.HasValue ? value.Value.ToString() : NullValue)}";
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this double? value )
        {
            return $"{(value.HasValue ? value.Value.ToString() : NullValue)}";
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">  The value. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this double? value, string format )
        {
            return value.ToDataString( format, NullValue );
        }

        /// <summary>
        /// Convert this object into a string representation that can be saved in the database.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="nullValue"> The null value. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToDataString( this double? value, string format, string nullValue )
        {
            return $"{(value.HasValue ? $"{value.Value.ToString( format )}" : nullValue)}";
        }
    }
}
