using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace isr.Core.TimeSpanExtensions
{
    /// <summary> Includes extensions for <see cref="TimeSpan"/> calculations. </summary>
    /// <remarks> Requires: DispatcherExtensions; Reference to Windows Base DLL. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class TimeSpanExtensionMethods
    {

        static TimeSpanExtensionMethods()
        {
            for (int i = 0; i < 3; i++ )
            {
                Stopwatch sw = Stopwatch.StartNew();
                Thread.Sleep( 1 );
                sw.Stop();
                if ( TimeSpanExtensionMethods.SystemClockResolution < sw.Elapsed )
                    TimeSpanExtensionMethods.SystemClockResolution = sw.Elapsed;
            }
        }

        #region " EQUALS "

        /// <summary>
        /// A TimeSpan extension method that checks if the two timespan values are equal within
        /// <paramref name="epsilon"/>.
        /// </summary>
        /// <remarks>   David, 2020-11-25. </remarks>
        /// <param name="leftHand">     The leftHand to act on. </param>
        /// <param name="rightHand">    The right hand. </param>
        /// <param name="epsilon">      The epsilon. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool Approximates( this TimeSpan leftHand, TimeSpan rightHand , TimeSpan epsilon)
        {
            return Math.Abs(leftHand.Subtract(rightHand).Ticks) <= epsilon.Ticks;
        }

        #endregion

        #region " EXACT TIMES "

        /// <summary> Gets or sets the microseconds per tick. </summary>
        /// <value> The microseconds per tick. </value>
        public static double MicrosecondsPerTick { get; private set; } = 1000000.0d / Stopwatch.Frequency;

        /// <summary> Gets or sets the millisecond per tick. </summary>
        /// <value> The millisecond per tick. </value>
        public static double MillisecondsPerTick { get; private set; } = 1000.0d / Stopwatch.Frequency;

        /// <summary> Gets or sets the seconds per tick. </summary>
        /// <value> The seconds per tick. </value>
        public static double SecondsPerTick { get; private set; } = 1.0d / TimeSpan.TicksPerSecond;

        /// <summary> Gets or sets the ticks per microseconds. </summary>
        /// <value> The ticks per microseconds. </value>
        public static double TicksPerMicroseconds { get; private set; } = 0.001d * TimeSpan.TicksPerMillisecond;

        /// <summary> Converts seconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromSecondsPrecise(42.042)
        /// </code>
        /// </remarks>
        /// <param name="seconds"> The number of seconds. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromSeconds( this double seconds )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * seconds) );
        }

        /// <summary> Converts a timespan to the seconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToSeconds( this TimeSpan timespan )
        {
            return timespan.Ticks * SecondsPerTick;
        }

        /// <summary> Converts milliseconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMillisecondsPrecise(42.042)
        /// </code>
        /// </remarks>
        /// <param name="milliseconds"> The number of milliseconds. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromMilliseconds( this double milliseconds )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * milliseconds) );
        }

        /// <summary> Converts a timespan to an exact milliseconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToMilliseconds( this TimeSpan timespan )
        {
            return timespan.Ticks * MillisecondsPerTick;
        }

        /// <summary> Converts microseconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMicroseconds(42.2)
        /// </code>
        /// </remarks>
        /// <param name="microseconds"> The value. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromMicroseconds( this double microseconds )
        {
            return TimeSpan.FromTicks( ( long ) (TicksPerMicroseconds * microseconds) );
        }

        /// <summary> Converts a timespan to an exact microseconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToMicroseconds( this TimeSpan timespan )
        {
            return timespan.Ticks * MicrosecondsPerTick;
        }

        /// <summary>
        /// A TimeSpan extension method that adds the microseconds to 'microseconds'.
        /// </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        /// <param name="self">         The self to act on. </param>
        /// <param name="microseconds"> The value. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan AddMicroseconds( this TimeSpan self , double microseconds )
        {
            return self.Add( TimeSpan.FromTicks( (long)( microseconds * TicksPerMicroseconds ) ) );
        }

        #endregion

        #region " DELAYS "

        /// <summary> The system clock resolution. </summary>
        /// <value> The system clock resolution. </value>
        public static TimeSpan SystemClockResolution { get; private set; } = (1000d / 64).FromMilliseconds();

        /// <summary>   The thread clock resolution. </summary>
        /// <remarks>
        /// This might have changed from previous test results with the upgrade to Windows 20H2. Test
        /// results consistently show the thread sleep resolution at 15.6 ms.
        /// https://stackoverflow.com/questions/7614936/can-i-improve-the-resolution-of-thread-sleep The
        /// Thread.Sleep cannot be expected to provide reliable timing. It is notorious for behaving
        /// differently on different hardware Thread.Sleep(1) could sleep for 15.6 ms.
        /// https://social.msdn.microsoft.com/Forums/vstudio/en-US/facc2b57-9a27-4049-bb32-ef093fbf4c29/threadsleep1-sleeps-for-156-ms?forum=clr.
        /// </remarks>
        /// <value> The thread clock resolution. </value>
        public static TimeSpan ThreadClockResolution { get; private set; } = 15.6001d.FromMilliseconds();

        /// <summary> The high resolution clock resolution. </summary>
        /// <value> The high resolution clock resolution. </value>
        public static TimeSpan HighResolutionClockResolution { get; private set; } = (1d / Stopwatch.Frequency).FromSeconds();

        /// <summary>
        /// A TimeSpan extension method that Lets Windows process all the messages currently in the
        /// message queue during a wait by invoking the wait on a task. 
        /// </summary>
        /// <remarks>
        /// David, 2020-11-05. Note that the first call to <see cref="ApplianceBase.DoEvents()"/> takes
        /// some extra 90 ms. <para>
        /// DoEventsWait(1ms)  waits: 00:00:00.0010066ms </para><para>
        /// DoEventsWait(2ms)  waits: 00:00:00.0020038ms </para><para>
        /// DoEventsWait(5ms)  waits: 00:00:00.0050051ms </para><para>
        /// DoEventsWait(10ms) waits: 00:00:00.0100138ms </para><para>
        /// DoEventsWait(20ms) waits: 00:00:00.0200103ms </para><para>
        /// DoEventsWait(50ms) waits: 00:00:00.0500064ms </para><para>
        /// DoEventsWait(100ms) waits: 00:00:00.1000037ms </para>
        /// </remarks>
        /// <param name="delayTime">    The delay time. </param>
        public static void DoEventsWait( this TimeSpan delayTime )
        {
            TimeSpanExtensionMethods.AsyncWait( delayTime );
        }

        /// <summary>
        /// A TimeSpan extension method that synchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution.
        /// </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between <see cref="Thread.Sleep(int)"/>
        ///                             and <see cref="Thread.SpinWait(int)"/>. </param>
        /// <returns>   The actual wait time. </returns>
        public static TimeSpan SyncWait( this TimeSpan delayTime, bool yield = false )
        {
            Stopwatch sw = Stopwatch.StartNew();
            var yieldCount = 100;
            var counter = yieldCount;
            int systemClockCycles = (int)Math.Floor( delayTime / TimeSpanExtensionMethods.SystemClockResolution );
            if ( systemClockCycles > 0 )
            {
                if ( yield )
                {
                    while ( delayTime.Subtract( sw.Elapsed ) < TimeSpanExtensionMethods.SystemClockResolution )
                    {
                        var ms = ( int ) Math.Floor( TimeSpanExtensionMethods.SystemClockResolution.TotalMilliseconds );
                        Thread.Sleep( ms );
                        _ = Thread.Yield();
                    }
                }
                else
                {
                    Thread.Sleep( delayTime );
                    _ = Thread.Yield();
                }
            }
            while ( sw.Elapsed < delayTime )
            {
                if ( counter >= yieldCount )
                {
                    Thread.SpinWait( 1 );
                    if ( yield )
                    {
                        _ = Thread.Yield();
                    }
                }
                counter = 0;
            }
            return sw.Elapsed;
        }

        /// <summary>   A TimeSpan extension method that starts a wait task. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A Task. </returns>
        public static Task StartWaitTask( this TimeSpan delayTime, bool yield = false )
        {
            return Task.Factory.StartNew( () => SyncWait( delayTime, yield ) );
        }

        /// <summary>   A TimeSpan extension method that asynchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        public static void AsyncWait( this TimeSpan delayTime, bool yield = false )
        {
            StartWaitTask( delayTime, yield ).Wait();
        }

        /// <summary>   A TimeSpan extension method that starts wait task returning the elapsed time. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A TimeSpan Task. </returns>
        public static Task<TimeSpan> StartWaitElpasedTask( this TimeSpan delayTime, bool yield = false )
        {
            return Task<TimeSpan>.Factory.StartNew( () => { return SyncWait( delayTime, yield ); } );
        }

        /// <summary>
        /// A TimeSpan extension method that asynchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution and returning the elapsed time.
        /// </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan AsyncWaitElapsed( this TimeSpan delayTime, bool yield = false )
        {
            System.Threading.Tasks.Task<TimeSpan> t = StartWaitElpasedTask( delayTime, yield );
            t.Wait();
            return t.Result;
        }

        #endregion

    }
}
