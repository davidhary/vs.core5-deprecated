// ---------------------------------------------------------------------------------------------------
// file:	.\Log\Logger_IMessageListener.cs
// 
// summary:	My log i message listener class
// ---------------------------------------------------------------------------------------------------
using System.Diagnostics;

using isr.Core.DiagnosticsExtensions;

namespace isr.Core
{
    public partial class Logger : IMessageListener
    {

        /// <summary> Registers this event. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="level"> The level. </param>
        public virtual void Register( TraceEventType level )
        {
        }

        /// <summary> Writes the message at the information level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message to add. </param>
        public void Write( string message )
        {
            this.WriteLine( message );
        }

        /// <summary> Writes a line. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="message"> The message to add. </param>
        public void WriteLine( string message )
        {
            _ = this.WriteLogEntry( TraceEventType.Information, My.MyLibrary.TraceEventId, message );
        }

        /// <summary> The trace level. </summary>
        private TraceEventType _TraceLevel;

        /// <summary> Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel
        {
            get => this._TraceLevel;

            set => this.ApplyTraceLevelThis( value );
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        private void ApplyTraceLevelThis( TraceEventType value )
        {
            // a private internal trace level is now used instead of the trace source to facilitate overridingthe trace source level.
            this._TraceLevel = value;
            base.TraceSource.ApplyTraceLevel( TraceEventType.Verbose );
        }

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        public void ApplyTraceLevel( TraceEventType value )
        {
            this.ApplyTraceLevelThis( value );
        }

        /// <summary> Gets the sentinel indicating a thread safe. </summary>
        /// <value> True if thread safe. </value>
        public virtual bool IsThreadSafe => true;

    }
}
