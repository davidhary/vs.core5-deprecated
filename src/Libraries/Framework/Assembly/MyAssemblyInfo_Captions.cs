﻿using System;

namespace isr.Core
{
    public partial class MyAssemblyInfo
    {

        /// <summary> Gets the application information caption with ISO 8601 UTC time, e.g., '2019-09-10 19:27:04Z'. </summary>
        /// <value> The UTC caption. </value>
        public string IsoUniversalTimeCaption => $"{this.PrefixProcessName()}.r.{this.ProductVersion( 4 )} {DateTimeOffset.Now:u)}";

        /// <summary> Gets the GMT caption, e.g., 'Tue, 10 May 2019 19:26:42 GMT'. </summary>
        /// <value> The GMT caption. </value>
        public string GmtCaption => $"{this.PrefixProcessName()}.r.{this.ProductVersion( 4 )} {DateTimeOffset.Now:r)}";

        /// <summary> Gets the application information caption with ISO complete time, e.g., '2019-09-10T12:12:29.7552627-07:00'. </summary>
        /// <value> The UTC caption. </value>
        public string IsoCompleteCaption => $"{this.PrefixProcessName()}.r.{this.ProductVersion( 4 )} {DateTimeOffset.Now:o)}";

        /// <summary> Gets the application ISO 8691 Short date and time caption, e.g., 2019-09-10T12:24:47-07:00. </summary>
        /// <value> The time caption. </value>
        public string IsoShortCaption => $"{this.PrefixProcessName()}.r.{this.ProductVersion( 4 )} {DateTimeOffset.Now:s)}{DateTimeOffset.Now:zzz)}";
    }
}