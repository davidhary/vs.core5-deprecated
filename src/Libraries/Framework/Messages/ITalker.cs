namespace isr.Core
{
    /// <summary> Interface for talker. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ITalker : IPublisher
    {

        /// <summary> Gets or sets the trace message talker. </summary>
        /// <value> The trace message talker. </value>
        ITraceMessageTalker Talker { get; }

        /// <summary> Assigns a talker. </summary>
        /// <param name="talker"> The talker. </param>
        void AssignTalker( ITraceMessageTalker talker );

        /// <summary> Identifies talkers. </summary>
        void IdentifyTalkers();
    }
}
