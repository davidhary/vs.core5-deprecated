using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core
{
    /// <summary>
    /// Trace Message Talker -- broadcasts trace messages to trace message listeners.
    /// </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-10-10 </para>
    /// </remarks>
    public class TraceMessageTalker : ITraceMessageTalker
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public TraceMessageTalker() : base()
        {
            // this prevents issuing new date messages.
            this.UpdateLastReportDate();
            this.Listeners = new MessageListenerCollection();
            this.TraceMessage = TraceMessage.Empty;
            this.TraceLogLevel = TraceEventType.Information;
            this.TraceShowLevel = TraceEventType.Information;
        }

        #endregion

        #region " LISTENERS "

        /// <summary> Adds a listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listener"> The listener. </param>
        public void AddListener( IMessageListener listener )
        {
            if ( listener is null )
            {
                throw new ArgumentNullException( nameof( listener ) );
            }

            this.Listeners.Add( listener );
        }

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public void AddListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.AddListeners( talker.Listeners );
        }

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public void AddListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            foreach ( IMessageListener listener in listeners )
            {
                this.AddListener( listener );
            }
        }

        /// <summary> Removes the listeners if the talker was not assigned. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void RemoveListeners()
        {
            this.Listeners?.Clear();
        }

        /// <summary> The private listeners. </summary>
        private List<IMessageListener> _PrivateListeners;

        /// <summary> Gets the private listeners. </summary>
        /// <value> The private listeners. </value>
        public IList<IMessageListener> PrivateListeners
        {
            get {
                if ( this._PrivateListeners is null )
                {
                    this._PrivateListeners = new List<IMessageListener>();
                }

                return this._PrivateListeners;
            }
        }

        /// <summary> Adds a private listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listener"> The listener. </param>
        public void AddPrivateListener( IMessageListener listener )
        {
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.Add( listener );
            }

            this.AddListener( listener );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public void AddPrivateListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.AddRange( listeners );
            }

            this.AddListeners( listeners );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public void AddPrivateListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.AddPrivateListeners( talker.Listeners );
        }

        /// <summary> Removes the private listener described by listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listener"> The listener. </param>
        public void RemovePrivateListener( IMessageListener listener )
        {
            this.RemoveListener( listener );
            if ( this.PrivateListeners is object )
            {
                _ = this._PrivateListeners.Remove( listener );
            }
        }

        /// <summary> Removes the private listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void RemovePrivateListeners()
        {
            foreach ( IMessageListener listener in this.PrivateListeners )
            {
                this.RemoveListener( listener );
            }

            this._PrivateListeners.Clear();
        }

        /// <summary> Removes the listener described by listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listener"> The listener. </param>
        public void RemoveListener( IMessageListener listener )
        {
            this.Listeners?.Remove( listener );
        }

        /// <summary> Removes the listeners if the talker was not assigned. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public void RemoveListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
            {
                throw new ArgumentNullException( nameof( listeners ) );
            }

            foreach ( IMessageListener listener in listeners )
            {
                this.RemoveListener( listener );
            }
        }

        /// <summary> Gets or sets the listeners. </summary>
        /// <value> The listeners. </value>
        public MessageListenerCollection Listeners { get; private set; }

        /// <summary> Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLogLevel { get; set; }

        /// <summary> Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceShowLevel { get; set; }

        /// <summary> Applies the listener trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this.Listeners.ApplyTraceLevel( listenerType, value );
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            if ( listenerType == ListenerType.Logger )
            {
                this.TraceLogLevel = value;
            }
            else
            {
                this.TraceShowLevel = value;
            }
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.ApplyTalkerTraceLevel( ListenerType.Logger, talker.TraceLogLevel );
            this.ApplyTalkerTraceLevel( ListenerType.Display, talker.TraceShowLevel );
        }

        /// <summary> Applies the listener trace levels described by talker. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            if ( talker is null )
            {
                throw new ArgumentNullException( nameof( talker ) );
            }

            this.ApplyListenerTraceLevel( ListenerType.Logger, talker.TraceLogLevel );
            this.ApplyListenerTraceLevel( ListenerType.Display, talker.TraceShowLevel );
        }

        #endregion

        #region " PUBLISH "

        /// <summary>
        /// Determines if the message with the specified level is publishable on the listener type.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        /// <returns> <c>true</c> if publishable; otherwise <c>false</c> </returns>
        public bool Publishable( ListenerType listenerType, TraceEventType value )
        {
            return listenerType == ListenerType.Logger ? value <= this.TraceLogLevel : value <= this.TraceShowLevel;
        }

        /// <summary> Publishes the message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public string Publish( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.Publish( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Publishes the message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        public string Publish( TraceMessage value )
        {
            string details = string.Empty;
            if ( value is object )
            {
                if ( this.IsNewReportDate() )
                {
                    this.HandleDateChange();
                }

                this.TraceMessage = new TraceMessage( value );
                details = value.Details;
                this.PublishThis( value );
            }

            return details;
        }

        /// <summary> Publishes the message (private access). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        private void PublishThis( TraceMessage value )
        {
            if ( this.Listeners is object )
            {
                foreach ( ITraceMessageListener listener in this.Listeners )
                {
                    if ( listener is object && !listener.IsDisposed && this.Publishable( listener.ListenerType, value.EventType ) )
                    {
                        _ = listener.TraceEvent( value );
                        ApplianceBase.DoEvents();
                    }
                }
            }
        }

        /// <summary> Publishes overriding the listeners trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public string PublishOverride( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.PublishOverride( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Publishes overriding the listeners trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        public string PublishOverride( TraceMessage value )
        {
            string details = string.Empty;
            if ( value is object )
            {
                if ( this.IsNewReportDate() )
                {
                    this.HandleDateChange();
                }

                this.TraceMessage = new TraceMessage( value );
                details = value.Details;
                this.PublishOverrideThis( value );
            }

            return details;
        }

        /// <summary> Publish override (private access). </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        private void PublishOverrideThis( TraceMessage value )
        {
            if ( this.Listeners is object )
            {
                foreach ( ITraceMessageListener listener in this.Listeners )
                {
                    if ( listener is object && !listener.IsDisposed )
                    {
                        _ = listener.TraceEventOverride( value );
                        ApplianceBase.DoEvents();
                    }
                }
            }
        }

        /// <summary> Gets or sets a message describing the trace. </summary>
        /// <value> A message describing the trace. </value>
        public TraceMessage TraceMessage { get; private set; }

        #endregion

        #region " IDENTIFY "

        /// <summary> Identify talker. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        public void IdentifyTalker( TraceMessage value )
        {
            if ( this.Listeners is object )
            {
                foreach ( ITraceMessageListener listener in this.Listeners )
                {
                    if ( listener is object && !listener.IsDisposed )
                    {
                        if ( !listener.IsTalkerIdentified( value ) )
                        {
                            _ = listener.TraceEventOverride( value );
                            listener.IdentifyTalker( value );
                            ApplianceBase.DoEvents();
                        }
                    }
                }
            }
        }

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void IdentifyTalkers()
        {
            if ( this.Listeners is object )
            {
                foreach ( ITraceMessageListener listener in this.Listeners )
                {
                    if ( listener is object && !listener.IsDisposed )
                    {
                        foreach ( TraceMessage value in listener.TalkerIdentityMessages() )
                        {
                            _ = listener.TraceEventOverride( value );
                            ApplianceBase.DoEvents();
                        }
                    }
                }
            }
        }

        #endregion

        #region " DATE CHANGED "

        /// <summary> Publishes date message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        /// <param name="id">        The identifier to use with the trace event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        public string PublishDateMessage( TraceEventType eventType, int id, string format, params object[] args )
        {
            return this.PublishDateMessage( new TraceMessage( eventType, id, format, args ) );
        }

        /// <summary> Publishes date message. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A String. </returns>
        public string PublishDateMessage( TraceMessage value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                if ( this.Listeners is object )
                {
                    foreach ( ITraceMessageListener listener in this.Listeners )
                    {
                        if ( listener is object && !listener.IsDisposed )
                        {
                            if ( !listener.IsTalkerDateMessagePublished( value ) )
                            {
                                _ = listener.TraceEventOverride( value );
                                listener.AddDateMessage( value );
                                ApplianceBase.DoEvents();
                            }
                        }
                    }
                }

                return value.Details;
            }
        }

        /// <summary> Publish date messages. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public void PublishDateMessages()
        {
            if ( this.Listeners is object )
            {
                foreach ( ITraceMessageListener listener in this.Listeners )
                {
                    if ( listener is object && !listener.IsDisposed )
                    {
                        foreach ( TraceMessage value in listener.TalkerDateMessages() )
                        {
                            _ = listener.TraceEventOverride( value );
                            ApplianceBase.DoEvents();
                        }
                    }
                }
            }
        }

        /// <summary> Updates the last report date. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void UpdateLastReportDate()
        {
            this.LastReportDate = DateTimeOffset.Now.Date;
        }

        /// <summary> Query if this object is new report date. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> <c>true</c> if new report date; otherwise <c>false</c> </returns>
        private bool IsNewReportDate()
        {
            return DateTimeOffset.Now.Date > this.LastReportDate;
        }

        /// <summary> Gets or sets the last report date in local time. </summary>
        /// <remarks>
        /// MS Logging using <see cref="Microsoft.VisualBasic.Logging.LogFileCreationScheduleOption.Daily">daily</see> file
        /// creation, creates the new file on a local date change. In congruence, the
        /// <see cref="LastReportDate">last report date</see> represents local time.
        /// </remarks>
        /// <value> The last report date in local time. </value>
        private DateTimeOffset LastReportDate { get; set; }

        /// <summary> Handles the date change. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private void HandleDateChange()
        {
            this.UpdateLastReportDate();
            // on a new date, publish all date messages and identify all talker
            this.PublishDateMessages();
            this.IdentifyTalkers();
        }

        #endregion

    }
}
