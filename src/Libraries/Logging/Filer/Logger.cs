// file:	Logging\Logger\Logger.cs
//
// summary:	Implements the logger class

using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

namespace isr.Core.Logging.Filer
{

    /// <summary>
    /// Represents an object that handles log information. <para>
    /// This class does NOT save log information in a medium. Its responsibility is to create a
    /// log info, fill the properties of that log info, and then passes it to the associated logger
    /// provider.</para>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    internal class Logger : ILogger
    {

        #region " CONSTRUCTION "
        /// <summary>
        /// Creates a logger instance per the category name for the specified provider. <para>
        /// CAUTION: You never create a logger directly. This is a responsibility of the logging
        /// framework by calling the provider's CreateLogger().</para>
        /// </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="provider"> The provider. </param>
        /// <param name="category"> The category name for messages produced by the logger. The category
        ///                         is typically the name of the logging source, such as the fully
        ///                         qualified name of a class asking for a logger, e.g.
        ///                         MyNamespace.MyClass. </param>
        public Logger( LoggerProvider provider, string category )
        {
            this.Provider = provider;
            this.Category = category;
        }

        #endregion

        #region " ILOGGER IMPLEMENTATION "

        /// <summary>
        /// Begins a logical operation scope. Returns an IDisposable that ends the logical operation
        /// scope on dispose.
        /// </summary>
        /// <typeparam name="TState">   The type of the state to begin scope for. </typeparam>
        /// <param name="state">    The identifier for the scope. </param>
        /// <returns>   A disposable object that ends the logical operation scope on dispose. </returns>
        IDisposable ILogger.BeginScope<TState>( TState state )
        {
            return this.Provider.ScopeProvider.Push( state );
        }

        /// <summary>   Checks if the given logLevel is enabled. </summary>
        /// <param name="logLevel"> level to be checked. </param>
        /// <returns>   <see langword="true" /> if enabled; <see langword="false" /> otherwise. </returns>
        bool ILogger.IsEnabled( LogLevel logLevel )
        {
            return this.Provider.IsEnabled( logLevel );
        }

        /// <summary>
        /// Creates a log entry, actually a log info instance, fill the properties of that log info, and
        /// then passes it to the associated logger provider. <para>
        /// WARNING: It's easier to use the ILogger extension methods than this method, since it
        /// requires a lot of parameters, so calling it could be a very complicated action.</para>
        /// </summary>
        /// <typeparam name="TState">   The type of the object to be written. </typeparam>
        /// <param name="logLevel">     Entry will be written on this level. </param>
        /// <param name="eventId">      Id of the event. </param>
        /// <param name="state">        The entry to be written. Can be also an object. </param>
        /// <param name="exception">    The exception related to this entry. </param>
        /// <param name="formatter">    Function to create a <see cref="T:System.String" /> message of
        ///                             the <paramref name="state" /> and <paramref name="exception" />. </param>
        void ILogger.Log<TState>( LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter )
        {
            // checks if this log entry needs to be logged.
            if ( (this as ILogger).IsEnabled( logLevel ) )
            {

                // well, the passed default formatter function does not take the exception into account
                // SEE:  https://github.com/aspnet/Extensions/blob/master/src/Logging/Logging.Abstractions/src/LoggerExtensions.cs

                LogEntry logEntry = new LogEntry {
                    Category = this.Category,
                    Level = logLevel,
                    Text = exception?.Message ?? state.ToString(), // formatter(state, exception)
                    Exception = exception,
                    EventId = eventId,
                    State = state
                };

                // well, you never know what it really is
                if ( state is string )
                {
                    logEntry.StateText = state.ToString();
                }

                // in case we have to do with a message template, lets get the keys and values (for Structured Logging providers)
                // SEE: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging#log-message-template
                // SEE: https://softwareengineering.stackexchange.com/questions/312197/benefits-of-structured-logging-vs-basic-logging
                else if ( state is IEnumerable<KeyValuePair<string, object>> Properties )
                {
                    logEntry.StateProperties = new Dictionary<string, object>();

                    foreach ( KeyValuePair<string, object> item in Properties )
                    {
                        logEntry.StateProperties[item.Key] = item.Value;
                    }
                }

                // gather info about scope(s), if any
                if ( this.Provider.ScopeProvider != null )
                {
                    this.Provider.ScopeProvider.ForEachScope( ( value, loggingProps ) => {
                        if ( logEntry.Scopes == null )
                            logEntry.Scopes = new List<LogScopeInfo>();

                        LogScopeInfo Scope = new LogScopeInfo();
                        logEntry.Scopes.Add( Scope );

                        if ( value is string )
                        {
                            Scope.Text = value.ToString();
                        }
                        else if ( value is IEnumerable<KeyValuePair<string, object>> props )
                        {
                            if ( Scope.Properties == null )
                                Scope.Properties = new Dictionary<string, object>();

                            foreach ( var pair in props )
                            {
                                Scope.Properties[pair.Key] = pair.Value;
                            }
                        }
                    },
                    state );

                }

                this.Provider.WriteLog( logEntry );

            }
        }

        /// <summary>   The logger provider who created this instance. </summary>
        /// <value> The provider. </value>
        public LoggerProvider Provider { get; private set; }

        /// <summary>
        /// The category that this instance serves. <para>
        /// The category is usually the fully qualified class name of a class asking for a logger,
        /// e.g. MyNamespace.MyClass </para>
        /// </summary>
        /// <value> The category. </value>
        public string Category { get; private set; }

        #endregion
    }
}
