// file:	Logging\Logger\LogScopeInfo.cs
//
// summary:	Implements the log scope information class

using System.Collections.Generic;

namespace isr.Core.Logging.Filer
{

    /// <summary>   Scope information regarding a log entry. </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    public class LogScopeInfo
    {

        /// <summary>   Constructor. </summary>
        public LogScopeInfo()
        {
        }

        /// <summary>   Used when the Scope is just a string type, else it is null. </summary>
        /// <value> The text. </value>
        public string Text { get; set; }

        /// <summary>   Used when the Scope is a Dictionary-like object. </summary>
        /// <value> The properties. </value>
        public Dictionary<string, object> Properties { get; set; }
    }
}
