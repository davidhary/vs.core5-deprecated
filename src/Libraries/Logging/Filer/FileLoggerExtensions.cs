// file:	Logging\Logger\FileLoggerExtensions.cs
//
// summary:	Implements the file logger extensions class

using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace isr.Core.Logging.Filer
{
    /// <summary>   File logger extension methods. </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    public static class FileLoggerExtensions
    {

        /// <summary>
        /// Adds the file logger provider, aliased as 'File', in the available services as singleton and
        /// binds the file logger options class to the 'File' section of the appsettings.json file.
        /// </summary>
        /// <param name="builder">  The builder to act on. </param>
        /// <returns>   An ILoggingBuilder. </returns>
        public static ILoggingBuilder AddFileLogger( this ILoggingBuilder builder )
        {
            builder.AddConfiguration();

            builder.Services.TryAddEnumerable( ServiceDescriptor.Singleton<ILoggerProvider, FileLoggerProvider>() );
            builder.Services.TryAddEnumerable( ServiceDescriptor.Singleton<IConfigureOptions<FileLoggerOptions>, FileLoggerOptionsSetup>() );
            builder.Services.TryAddEnumerable( ServiceDescriptor.Singleton<IOptionsChangeTokenSource<FileLoggerOptions>, LoggerProviderOptionsChangeTokenSource<FileLoggerOptions, FileLoggerProvider>>() );
            return builder;
        }

        /// <summary>
        /// Adds the file logger provider, aliased as 'File', in the available services as singleton and
        /// binds the file logger options class to the 'File' section of the appsettings.json file.
        /// </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="builder">      The builder to act on. </param>
        /// <param name="configure">    The configure. </param>
        /// <returns>   An ILoggingBuilder. </returns>
        public static ILoggingBuilder AddFileLogger( this ILoggingBuilder builder, Action<FileLoggerOptions> configure )
        {
            if ( configure == null )
            {
                throw new ArgumentNullException( nameof( configure ) );
            }

            _ = builder.AddFileLogger();
            _ = builder.Services.Configure( configure );

            return builder;
        }
    }
}
