// file:	Logging\Logger\LoggerProvider.cs
//
// summary:	Implements the logger provider class

using System;

using System.Collections.Concurrent;

using Microsoft.Extensions.Logging;

namespace isr.Core.Logging.Filer
{

    /// <summary>
    /// A base logger provider class. <para>
    /// A logger provider essentially represents the medium where log information is saved.</para>
    /// <para>This class may serve as base class in writing a file or database logger provider.</para>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    public abstract class LoggerProvider : IDisposable, ILoggerProvider, ISupportExternalScope
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Constructor. </summary>
        public LoggerProvider()
        {
        }

        /// <summary>   Returns true when this instance is disposed. </summary>
        /// <value> True if this object is disposed, false if not. </value>
        public bool IsDisposed { get; protected set; }

        /// <summary>   Disposes the options change toker. IDisposable pattern implementation. </summary>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.SettingsChangeToken != null )
            {
                this.SettingsChangeToken.Dispose();
                this.SettingsChangeToken = null;
            }
            this._Loggers.Clear();
        }

        /// <summary>   Disposes this instance. </summary>
        void IDisposable.Dispose()
        {
            if ( !this.IsDisposed )
            {
                try
                {
                    this.Dispose( true );
                }
                catch
                {
                }

                this.IsDisposed = true;
                GC.SuppressFinalize( this );  // instructs GC not bother to call the destructor                
            }
        }

        /// <summary>   Destructor. </summary>
        ~LoggerProvider()
        {
            if ( !this.IsDisposed )
            {
                this.Dispose( false );
            }
        }

        #endregion

        #region " PUBLIC " 

        /// <summary>
        /// Returns true if a specified log level is enabled. <para>
        /// Called by logger instances created by this provider.</para>
        /// </summary>
        /// <param name="logLevel"> The log level. </param>
        /// <returns>   True if enabled, false if not. </returns>
        public abstract bool IsEnabled( LogLevel logLevel );

        /// <summary>
        /// The loggers do not actually log the information in any medium. Instead the call their
        /// provider WriteLog() method, passing the gathered log information.
        /// </summary>
        /// <param name="info"> The information. </param>
        public abstract void WriteLog( LogEntry info );

        /// <summary>   Gets or sets the settings change token. </summary>
        /// <value> The settings change token. </value>
        protected IDisposable SettingsChangeToken { get; set; }

        #endregion

        #region " interface implementations " 

        /// <summary>   The scope provider. </summary>
        private IExternalScopeProvider _ScopeProvider;

        /// <summary>
        /// Returns the scope provider. <para>
        /// Called by logger instances created by this provider.</para>
        /// </summary>
        /// <value> The scope provider. </value>
        internal IExternalScopeProvider ScopeProvider
        {
            get {
                if ( this._ScopeProvider == null )
                    this._ScopeProvider = new LoggerExternalScopeProvider();
                return this._ScopeProvider;
            }
        }

        /// <summary>
        /// Called by the logging framework in order to set external scope information source for the
        /// logger provider. <para>
        /// <see cref="ISupportExternalScope"/> implementation</para>
        /// </summary>
        /// <param name="scopeProvider">    The provider of scope data. </param>
        void ISupportExternalScope.SetScopeProvider( IExternalScopeProvider scopeProvider )
        {
            this._ScopeProvider = scopeProvider;
        }

        /// <summary>   The loggers. </summary>
        private readonly ConcurrentDictionary<string, Logger> _Loggers = new ConcurrentDictionary<string, Logger>();

        /// <summary>
        /// Creates a single instance of the <see cref="Logger"/> to serve the specified
        /// <paramref name="category"/> name and stores it in a
        /// <see cref="ConcurrentDictionary{TKey, TValue}"/>.
        /// </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="category"> The category name for messages produced by the logger. The category
        ///                         is typically the name of the logging source, such as the fully
        ///                         qualified name of a class asking for a logger, e.g.
        ///                         MyNamespace.MyClass. </param>
        /// <returns>
        /// The instance of <see cref="T:Microsoft.Extensions.Logging.ILogger" /> that was created.
        /// </returns>
        ILogger ILoggerProvider.CreateLogger( string category )
        {
            return this._Loggers.GetOrAdd( category, ( category ) => { return new Logger( this, category ); } );
        }

        #endregion

    }


}
