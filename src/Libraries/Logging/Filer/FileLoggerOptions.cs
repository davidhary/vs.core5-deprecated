// file:	Logging\Logger\FileLoggerOptions.cs
//
// summary:	Implements the file logger options class

using Microsoft.Extensions.Logging;

namespace isr.Core.Logging.Filer
{

    /// <summary>
    /// Options for the file logger. <para>
    /// There are two ways to configure file logger: 1. using the ConfigureLogging() in
    /// Program.cs or using the appsettings.json file.</para> <para>
    /// 1. ConfigureLogging()</para>
    /// <code>
    /// .ConfigureLogging(logging =&gt;
    /// {
    ///     logging.ClearProviders();
    ///     // logging.AddFileLogger();
    ///     logging.AddFileLogger(options =&gt; {  
    ///         options.MaxFileSizeInMB = 5;
    ///     });
    /// })
    /// </code> <para>
    /// 2. appsettings.json file </para>
    /// <code>
    ///   "Logging": {
    ///     "LogLevel": {
    ///       "Default": "Warning"
    ///     },
    ///     "File": {
    ///       "LogLevel": "Debug",
    ///       "MaxFileSizeInMB": 5
    ///     }
    ///   },
    /// </code>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    public class FileLoggerOptions
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        public FileLoggerOptions()
        {
        }

        #endregion

        /// <summary>   The active log level. Defaults to LogLevel.Information. </summary>
        /// <value> The log level. </value>
        public LogLevel LogLevel { get; set; } = Microsoft.Extensions.Logging.LogLevel.Information;

        /// <summary>   Pathname of the folder. </summary>
        private string _Folder;

        /// <summary>
        /// The folder where log files should be placed. Defaults to this Assembly location.
        /// </summary>
        /// <value> The pathname of the folder. </value>
        public string Folder
        {
            get => !string.IsNullOrWhiteSpace( this._Folder ) ? this._Folder : System.IO.Path.GetDirectoryName( this.GetType().Assembly.Location );
            set => this._Folder = value;
        }

        /// <summary>   The maximum file size in megabytes. </summary>
        private int _MaxFileSizeInMB;

        /// <summary>   The maximum number in MB of a single log file. Defaults to 2. </summary>
        /// <value> The maximum file size in megabytes. </value>
        public int MaxFileSizeInMB
        {
            get => this._MaxFileSizeInMB > 0 ? this._MaxFileSizeInMB : 2;
            set => this._MaxFileSizeInMB = value;
        }

        /// <summary>   Number of retain policy files. </summary>
        private int _RetainPolicyFileCount;

        /// <summary>   The maximum number of log files to retain. Defaults to 5. </summary>
        /// <value> The number of retain policy files. </value>
        public int RetainPolicyFileCount
        {
            get => this._RetainPolicyFileCount < 5 ? 5 : this._RetainPolicyFileCount;
            set => this._RetainPolicyFileCount = value;
        }

    }
}
