// file:	Logging\Logger\LogEntry.cs
//
// summary:	Implements the log entry class

using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

namespace isr.Core.Logging.Filer
{
    /// <summary>
    /// The information of a log entry. <para>
    /// The logger creates an instance of this class when its Log() method is called, fills the
    /// properties and then passes the info to the provider calling WriteLog(). </para>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    public class LogEntry
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        public LogEntry()
        {
            this.TimeStampUtc = DateTime.UtcNow;
            this.UserName = Environment.UserName;
        }

        #endregion

        #region " FUNCTIONALITY " 

        /// <summary>   Returns the host name of the local computer. </summary>
        public static readonly string StaticHostName = System.Net.Dns.GetHostName();

        /// <summary>   User name of the person who is currently logged on to operating system. </summary>
        /// <value> The name of the user. </value>
        public string UserName { get; private set; }

        /// <summary>   Host name of local computer. </summary>
        /// <value> The name of the host. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string HostName => StaticHostName;

        /// <summary>   Date and time, in UTC, of the creation time of this instance. </summary>
        /// <value> The time stamp UTC. </value>
        public DateTime TimeStampUtc { get; private set; }

        /// <summary>
        /// Category to which this instance belongs. <para>
        /// The category is usually the fully qualified class name of a class asking for a logger,
        /// e.g. MyNamespace.MyClass </para>
        /// </summary>
        /// <value> The category. </value>
        public string Category { get; set; }

        /// <summary>   The log level of this information. </summary>
        /// <value> The level. </value>
        public LogLevel Level { get; set; }

        /// <summary>   The message of this information. </summary>
        /// <value> The text. </value>
        public string Text { get; set; }

        /// <summary>   The exception this information represents, if any, else null. </summary>
        /// <value> The exception. </value>
        public Exception Exception { get; set; }

        /// <summary>
        /// The EventId of this information. <para>
        /// An EventId with Id set to zero, usually means no EventId.</para>
        /// </summary>
        /// <value> The identifier of the event. </value>
        public EventId EventId { get; set; }

        /// <summary>
        /// The state object. Contains information regarding the text message. <para>
        /// It looks like its type is always <see cref="Microsoft.Extensions.Logging.FormattedLogValues"/>
        /// </para>
        /// </summary>
        /// <value> The state. </value>
        public object State { get; set; }

        /// <summary>   Used when State is just a string type. So far null. </summary>
        /// <value> The state text. </value>
        public string StateText { get; set; }

        /// <summary>
        /// A dictionary with State properties. <para>
        /// When the log message is a message template with format values, e.g.
        /// <code>Logger.LogInformation("Customer {CustomerId} order {OrderId} is completed", CustomerId, OrderId)</code>  </para>
        /// this dictionary contains entries gathered from the message in order to ease any Structured Logging providers.
        /// </summary>
        /// <value> The state properties. </value>
        public Dictionary<string, object> StateProperties { get; set; }

        /// <summary>   The scopes currently in use, if any. The last scope is. </summary>
        /// <value> The scopes. </value>
        public List<LogScopeInfo> Scopes { get; set; }

        #endregion 

    }
}
