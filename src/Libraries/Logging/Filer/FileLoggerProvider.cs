// file:	Logging\Logger\FileLoggerProvider.cs
//
// summary:	Implements the file logger provider class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;

using System.Collections.Concurrent;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace isr.Core.Logging.Filer
{
    /// <summary>
    /// A logger provider that writes log entries to a text file. <para>
    /// "File" is the provider alias of this provider and can be used in the Logging section of the
    /// appsettings.json.</para>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    [Microsoft.Extensions.Logging.ProviderAlias( "File" )]
    public class FileLoggerProvider : LoggerProvider
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor. <para>
        /// The <see cref="Microsoft.Extensions.Options.IOptionsMonitor{TOptions}"/>  provides the
        /// OnChange() method which is called when the user alters the settings of this provider in the
        /// appsettings.json file.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="settings"> The settings. </param>
        public FileLoggerProvider( IOptionsMonitor<FileLoggerOptions> settings )
            : this( settings.CurrentValue )
        {
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/change-tokens
            this.SettingsChangeToken = settings.OnChange( settings => {
                this.Settings = settings;
            } );
        }

        /// <summary>   Constructor. </summary>
        /// <param name="settings"> The settings. </param>
        public FileLoggerProvider( FileLoggerOptions settings )
        {
            this.PrepareLengths();
            this.Settings = settings;

            // TO_DO: Add to settings. 
            this.LoggingFormat = LogFormat.Columnar;
            this.TimestampFormat = "yyyy-MM-dd HH:mm:ss.ff";
            this.FileNameTimestampFormat = "yyyyMMdd-HHmm";

            // create the first file
            this.BeginFile();

            this.ThreadProc();
        }

        /// <summary>   Disposes the options change toker. IDisposable pattern implementation. </summary>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            this._Terminated = true;
            base.Dispose( disposing );
        }

        #endregion

        #region " FUNCTIONALITY "

        /// <summary>   True if terminated. </summary>
        private bool _Terminated;

        /// <summary>   Full pathname of the file. </summary>
        public string FilePath { get; private set; }

        /// <summary>   Queue of informations. </summary>
        private readonly ConcurrentQueue<LogEntry> _InfoQueue = new ConcurrentQueue<LogEntry>();

        /// <summary>   Applies the log file retainment policy according to options. </summary>
        private void ApplyRetainPolicy()
        {
            FileInfo FI;
            try
            {
                List<FileInfo> FileList = new DirectoryInfo( this.Settings.Folder )
                .GetFiles( "*.log", SearchOption.TopDirectoryOnly )
                .OrderBy( fi => fi.CreationTime )
                .ToList();

                while ( FileList.Count >= this.Settings.RetainPolicyFileCount )
                {
                    FI = FileList.First();
                    FI.Delete();
                    _ = FileList.Remove( FI );
                }
            }
            catch
            {
            }

        }

        /// <summary>   The counter. </summary>
        private int _Counter = 0;

        /// <summary>
        /// Writes a line of text to the current file. If the file reaches the size limit, creates a new
        /// file and uses that new file.
        /// </summary>
        /// <param name="text"> The text. </param>
        private void WriteLine( string text )
        {
            if ( DateTime.UtcNow.Date > this.FileCreationDate.Date )
            {
                this.BeginFile();
            }
            else
            {
                // check the file size after any 100 writes
                this._Counter++;
                if ( this._Counter % 100 == 0 )
                {
                    FileInfo FI = new FileInfo( this.FilePath );
                    if ( FI.Length > (1024 * 1024 * this.Settings.MaxFileSizeInMB) )
                    {
                        this.BeginFile();
                    }
                }
            }
            File.AppendAllText( this.FilePath, text );
        }

        /// <summary>
        /// Pads a string with spaces to a max length. Truncates the string to max length if the string
        /// exceeds the limit.
        /// </summary>
        /// <param name="text">         The text. </param>
        /// <param name="maxLength">    The maximum length of the. </param>
        /// <returns>   A string. </returns>
        private static string Pad( string text, int maxLength )
        {
            return 0 == maxLength
                ? text
                : string.IsNullOrWhiteSpace( text )
                    ? "".PadRight( maxLength )
                    : text.Length > maxLength
                        ? text.Substring( 0, maxLength )
                        : text.PadRight( maxLength );
        }

        /// <summary>   Values that represent column names. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private enum ColumnName
        {
            Time,
            Host,
            User,
            Level,
            EventId,
            Category,
            Scope,
            Text
        }

        /// <summary>   The lengths. </summary>
        private Dictionary<ColumnName, int> _Lengths;

        /// <summary>   Prepares the lengths of the columns in the log file. </summary>
        private void PrepareLengths()
        {
            // prepare the lengths table
            this._Lengths = new Dictionary<ColumnName, int> {
                { ColumnName.Time, 24 },
                { ColumnName.Host, 16 },
                { ColumnName.User, 16 },
                { ColumnName.Level, 14 },
                { ColumnName.EventId, 32 },
                { ColumnName.Category, 92 },
                { ColumnName.Scope, 64 },
                { ColumnName.Text, 0 }
            };
        }

        /// <summary>   Values that represent log formats. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        private enum LogFormat
        {
            Columnar,
            RowWise,
            TabSeparated
        }

        /// <summary>   Appends a value. </summary>
        /// <remarks>   David, 2021-02-02. </remarks>
        /// <param name="builder">      The builder. </param>
        /// <param name="logFormat">    The log format. </param>
        /// <param name="column">       The column. </param>
        /// <param name="value">        The value. </param>
        private void AppendValue( StringBuilder builder, LogFormat logFormat, ColumnName column, string value )
        {
            if ( LogFormat.Columnar == logFormat )
            {
                _ = builder.Append( Pad( value, this._Lengths[column] ) );
            }
            else
            {
                // defaults to CSV format.
                _ = builder.Append( $",{value}" );
            }
        }

        private string BuildHeader( LogFormat logFromat )
        {
            StringBuilder builder = new StringBuilder();
            foreach ( ColumnName suit in ( ColumnName[] ) Enum.GetValues( typeof( ColumnName ) ) )
            {
                this.AppendValue( builder, logFromat, suit, suit.ToString() );
            }
            return builder.ToString();
        }

        /// <summary>   Gets or sets the timestamp format. </summary>
        /// <value> The timestamp format. </value>
        public String TimestampFormat { get; set; }

        /// <summary>   Gets or sets the file name timestamp format. </summary>
        /// <value> The file name timestamp format. </value>
        public String FileNameTimestampFormat { get; set; }

        /// <summary>   Gets or sets the logging format. </summary>
        /// <value> The logging format. </value>
        private LogFormat LoggingFormat { get; set; }

        /// <summary>   Gets or sets the file creation date. </summary>
        /// <value> The file creation date. </value>
        private DateTime FileCreationDate { get; set; }

        /// <summary>   Creates a new disk file and writes the column titles. </summary>
        private void BeginFile()
        {
            _ = Directory.CreateDirectory( this.Settings.Folder );
            this.FileCreationDate = DateTime.UtcNow;
            this.FilePath = Path.Combine( this.Settings.Folder, $"{LogEntry.StaticHostName}-{this.FileCreationDate.ToString( this.FileNameTimestampFormat )}.log" );
            File.WriteAllText( this.FilePath, this.BuildHeader( this.LoggingFormat ) );
            this.ApplyRetainPolicy();
        }

        // No need to create a new array each time
        private static readonly char[] NewLineChars = Environment.NewLine.ToCharArray();

        /// <summary>
        /// Pops a log info instance from the stack, prepares the text line, and writes the line to the
        /// text file.
        /// </summary>
        private void WriteLogLine()
        {

            if ( this._InfoQueue.TryDequeue( out LogEntry Info ) )
            {
                StringBuilder builder = new StringBuilder();
                this.AppendValue( builder, this.LoggingFormat, ColumnName.Time, Info.TimeStampUtc.ToLocalTime().ToString( this.TimestampFormat ) );
                this.AppendValue( builder, this.LoggingFormat, ColumnName.Host, Info.HostName );
                this.AppendValue( builder, this.LoggingFormat, ColumnName.User, Info.UserName );
                this.AppendValue( builder, this.LoggingFormat, ColumnName.Level, Info.Level.ToString() );
                this.AppendValue( builder, this.LoggingFormat, ColumnName.EventId, Info.EventId == 0 ? String.Empty : Info.EventId.Name );
                this.AppendValue( builder, this.LoggingFormat, ColumnName.Category, Info.Category );

                string scopeText = string.Empty;
                if ( Info.Scopes != null && Info.Scopes.Count > 0 )
                {
                    LogScopeInfo scopeInfo = Info.Scopes.Last();
                    if ( !string.IsNullOrWhiteSpace( scopeInfo.Text ) )
                    {
                        scopeText = scopeInfo.Text;
                    }
                    else
                    {
                    }
                }
                this.AppendValue( builder, this.LoggingFormat, ColumnName.Scope, scopeText );

                string payload = Info.Text;

                /* writing properties is too much for a text file logger
                if (Info.StateProperties != null && Info.StateProperties.Count > 0)
                {
                    Text = Text + " Properties = " + Newtonsoft.Json.JsonConvert.SerializeObject(Info.StateProperties);
                }                 
                */

                if ( !string.IsNullOrWhiteSpace( payload ) )
                {
                    this.AppendValue( builder, this.LoggingFormat, ColumnName.Text, payload.TrimEnd( NewLineChars ) );
                }

                _ = builder.AppendLine();
                this.WriteLine( builder.ToString() );
            }

        }

        /// <summary>   Thread process. </summary>
        private void ThreadProc()
        {
            _ = Task.Run( () => {

                while ( !this._Terminated )
                {
                    try
                    {
                        this.WriteLogLine();
                        System.Threading.Thread.Sleep( 100 );
                    }
                    catch // (Exception ex)
                    {
                    }
                }

            } );
        }

        /// <summary>   Checks if the given logLevel is enabled. It is called by the Logger. </summary>
        /// <param name="logLevel"> The log level. </param>
        /// <returns>   True if enabled, false if not. </returns>
        public override bool IsEnabled( LogLevel logLevel )
        {
            return logLevel != LogLevel.None && this.Settings.LogLevel != LogLevel.None
                   && Convert.ToInt32( logLevel ) >= Convert.ToInt32( this.Settings.LogLevel );
        }

        /// <summary>   Writes the specified log information to a log file. </summary>
        /// <param name="info"> The information. </param>
        public override void WriteLog( LogEntry info )
        {
            this._InfoQueue.Enqueue( info );
        }

        /// <summary>   Returns the settings. </summary>
        /// <value> The settings. </value>
        internal FileLoggerOptions Settings { get; private set; }

        #endregion

    }
}
