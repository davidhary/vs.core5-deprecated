// file:	Logging\Logger\FileLoggerOptionsSetup.cs
//
// summary:	Implements the file logger options setup class

using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging.Configuration;

namespace isr.Core.Logging.Filer
{

    /// <summary>
    /// Configures a FileLoggerOptions instance by using ConfigurationBinder.Bind against an
    /// IConfiguration. <para>
    /// This class essentially binds a FileLoggerOptions instance with a section in the
    /// appsettings.json file.</para>
    /// </summary>
    /// <remarks>   Theodoros Bebekis, 2019-04-14. </remarks>
    internal class FileLoggerOptionsSetup : ConfigureFromConfigurationOptions<FileLoggerOptions>
    {

        /// <summary>   Constructor that takes the IConfiguration instance to bind against. </summary>
        /// <param name="providerConfiguration">    The provider configuration. </param>
        public FileLoggerOptionsSetup( ILoggerProviderConfiguration<FileLoggerProvider> providerConfiguration )
            : base( providerConfiguration.Configuration )
        {
        }
    }
}
