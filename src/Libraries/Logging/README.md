# Logging Framework

The logging framework contains the following parts:

## Log Level Enumeration
Includes three levels. It is easy to add more levels if needed.

## Log Entry
A class that serves as a container for a unit of log information.

## Log Listener 

An interface with just a single method, ProcessLog(LogEntry Info). 
That method is called whenever a new Entry is logged.

## Logger 

A static Logger class which provides log methods: Info(), Warn() and Error(). 

Logger is also an ILogListener register. It calls all registered listeners,
asynchronously (that is using a thread), whenever any of its log methods
is called and a new LogEntry is generated.

## Log File Listener 

There is also a fully functional LogFileListener class which saves log information to a text file, 
and also provides settings for log file retention policy.

## Support for Scopes

This logging system does not support Scopes, as is supported by Asp.Net Core.

It's easy to implement it though. All you need is another one Logger method, 
getting as parameter a string or whatever representing the Scope and 
returning an interface with log methods, more or less, 
similar to the log methods of the static Logger class.
