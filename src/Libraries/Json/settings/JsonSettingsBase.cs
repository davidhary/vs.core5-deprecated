using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

namespace isr.Core.Json
{

    /// <summary>   A JSON settings base. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    public abstract class JsonSettingsBase : System.ComponentModel.INotifyPropertyChanged
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="settingsFileName"> Filename of the settings file. </param>
        public JsonSettingsBase( string settingsFileName )
        {
            this._SettingsFullFileName = settingsFileName;
        }

        private string _SectionName;

        /// <summary>   Section name. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   A string. </returns>
        public string SectionName()
        {
            if ( string.IsNullOrEmpty( this._SectionName ) )
            {
                var attribe = ( SettingsSectionAttribute ) this.GetType().GetCustomAttributes( typeof( SettingsSectionAttribute ), true ).FirstOrDefault();
                this._SectionName = attribe is object ? attribe.SectionName : this.GetType().Name;
            }
            return this._SectionName;
        }

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        public void ReadSettings()
        {
            this.ReadSettings( this.SectionName() );
        }

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="settingsSectionName">  Name of the settings section. </param>
        public void ReadSettings( string settingsSectionName )
        {
            if ( this.Exists() )
            {
                var config = this.BuildConfig();
                config.GetSection( settingsSectionName ).Bind( this );
            }
        }

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="allSettings">  all settings. </param>
        public void SaveSettings(object allSettings )
        {
            JsonSettingsBase.SaveSettings( this._SettingsFullFileName, allSettings );
        }

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="fileName">     Filename of the file. </param>
        /// <param name="allSettings">  all settings. </param>
        public static void SaveSettings( string fileName,  object allSettings )
        {
            JsonSerializer serializer = new JsonSerializer {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented
            };

            using System.IO.StreamWriter sw = new System.IO.StreamWriter( fileName );
            using JsonWriter writer = new JsonTextWriter( sw );
            serializer.Serialize( writer, allSettings );
        }

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this, new PropertyChangedEventArgs( propertyName ) );
            }
        }

        private readonly string _SettingsFullFileName;

        /// <summary>   Settings full file name. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   A string. </returns>
        public string SettingsFullFileName()
        {
            return this._SettingsFullFileName;
        }

        /// <summary>   Builds application settings file title and extension. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assemblyTitle"> The assembly file title. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppSettingsFileName(string assemblyTitle)
        {
            return $"{assemblyTitle}.settings.json";
        }

        /// <summary>   Builds application settings file name. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   A string. </returns>
        public static string BuildAppSettingsFileName()
        {
            var fileName = JsonSettingsBase.BuildAppSettingsFileName( System.Reflection.Assembly.GetCallingAssembly().GetName().Name );
            return System.IO.Path.Combine( AppDomain.CurrentDomain.BaseDirectory, fileName ) ;
        }

        /// <summary>   Checks if the settings file exists. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   True if it exists; otherwise, false. </returns>
        public bool Exists()
        {
            System.IO.FileInfo fi = new System.IO.FileInfo( this._SettingsFullFileName );
            return fi.Exists;
        }

        /// <summary>   Builds the configuration. </summary>
        /// <remarks>   David, 2021-01-28. </remarks>
        /// <returns>   An IConfigurationRoot. </returns>
        private IConfigurationRoot BuildConfig()
        {
            System.IO.FileInfo fi = new System.IO.FileInfo( this._SettingsFullFileName );
            var config = new ConfigurationBuilder()
                .SetBasePath( fi.DirectoryName )
                .AddJsonFile( fi.Name, false, true ).Build();
            return config;
        }

    }

    /// <summary>   Attribute for settings section. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    [System.AttributeUsage( System.AttributeTargets.Class ) ]
    public class SettingsSectionAttribute : System.Attribute
    {
        /// <summary>   Gets or sets the name of the section. </summary>
        /// <value> The name of the section. </value>
        public string SectionName { get; private set; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="sectionName">  The name of the section. </param>
        public SettingsSectionAttribute( string sectionName )
        {
            this.SectionName = sectionName;
        }
    }
}
